# finance-tracker UI

This is the finance-tracker UI (built with Svelte and Vite).

## Development

Download the repo and run the appropriate console commands:

```sh
# install dependencies
npm install

# start a dev server with hot reload at localhost:3000
npm run dev

# or generate production ready bundle in dist/ directory
npm run build
```
