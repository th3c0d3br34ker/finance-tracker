import { writable } from "svelte/store";

export const activeMenu = writable("dashboard");

export const pageTitle = writable("");

export const appName = writable("");

export const hideControls = writable(false);
