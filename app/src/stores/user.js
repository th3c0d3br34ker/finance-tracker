import { writable } from "svelte/store";

export const USER_LOCAL_STORAGE_KEY = "user";

export function loadUserFromLocalStorage() {
  const userJson = localStorage.getItem(USER_LOCAL_STORAGE_KEY);
  return userJson ? JSON.parse(userJson) : null;
}

function saveUserToLocalStorage(user) {
  localStorage.setItem(USER_LOCAL_STORAGE_KEY, JSON.stringify(user));
}

// logged app admin
export const user = writable(loadUserFromLocalStorage() || {});

user.subscribe((value) => {
  saveUserToLocalStorage(value);
});

export function setUser(model) {
  user.set(model || {});
}
