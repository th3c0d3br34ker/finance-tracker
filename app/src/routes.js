import { wrap } from "svelte-spa-router/wrap";
import PageIndex from "@/components/PageIndex.svelte";
// import PageLogs from "@/components/logs/PageLogs.svelte";
// import PageRecords from "@/components/records/PageRecords.svelte";
// import PageAdmins from "@/components/admins/PageAdmins.svelte";
// import PageApplication from "@/components/settings/PageApplication.svelte";
// import PageMail from "@/components/settings/PageMail.svelte";
// import PageStorage from "@/components/settings/PageStorage.svelte";
// import PageAuthProviders from "@/components/settings/PageAuthProviders.svelte";
// import PageTokenOptions from "@/components/settings/PageTokenOptions.svelte";
// import PageExportCollections from "@/components/settings/PageExportCollections.svelte";
// import PageImportCollections from "@/components/settings/PageImportCollections.svelte";

const routes = {
  "/login": wrap({
    asyncComponent: () => import("@/components/auth/PageLogin.svelte"),
    userData: { showAppSidebar: false },
  }),

  //   "/request-password-reset": wrap({
  //     asyncComponent: () => import("@/components/admins/PageAdminRequestPasswordReset.svelte"),
  //     conditions: baseConditions.concat([(_) => !ApiClient.authStore.isValid]),
  //     userData: { showAppSidebar: false },
  //   }),

  //   "/confirm-password-reset/:token": wrap({
  //     asyncComponent: () => import("@/components/admins/PageAdminConfirmPasswordReset.svelte"),
  //     conditions: baseConditions.concat([(_) => !ApiClient.authStore.isValid]),
  //     userData: { showAppSidebar: false },
  //   }),

  "/dashboard": wrap({
    asyncComponent: () => import("@/components/dashboard/PageDashboard.svelte"),
    userData: { showAppSidebar: true },
  }),

  // ---------------------------------------------------------------
  // Transactions actions
  // ---------------------------------------------------------------

  "/transactions": wrap({
    asyncComponent: () =>
      import("@/components/transactions/PageTransactions.svelte"),
    userData: { showAppSidebar: true },
  }),

  // ---------------------------------------------------------------
  // Settings actions
  // ---------------------------------------------------------------

  "/settings": wrap({
    asyncComponent: () =>
      import("@/components/settings/PageApplication.svelte"),
    userData: { showAppSidebar: true },
  }),

  // ---------------------------------------------------------------
  // Records email confirmation actions
  // ---------------------------------------------------------------

  // catch-all fallback
  "*": wrap({
    component: PageIndex,
    userData: { showAppSidebar: false },
  }),
};

export default routes;
