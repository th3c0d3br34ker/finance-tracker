import instance from "./axiosInstance";

function getTransactions() {
  return instance
    .get("/transaction")
    .then((response) => response.data)
    .then((d) => {
      if (d.success) {
        return d.data;
      } else {
        d.message;
      }
    });
}

export default { getTransactions };
