// ---
import user from "./user";
import transactions from "./transactions";

// /**
//  * Clears the authorized state and redirects to the login page.
//  *
//  * @param {Boolean} [redirect] Whether to redirect to the login page.
//  */
// PocketBase.prototype.logout = function (redirect = true) {
//   this.authStore.clear();

//   if (redirect) {
//     replace("/login");
//   }
// };

// /**
//  * Generic API error response handler.
//  *
//  * @param  {Error}   err        The API error itself.
//  * @param  {Boolean} notify     Whether to add a toast notification.
//  * @param  {String}  defaultMsg Default toast notification message if the error doesn't have one.
//  */
// PocketBase.prototype.errorResponseHandler = function (err, notify = true, defaultMsg = "") {
//   if (!err || !(err instanceof Error) || err.isAbort) {
//     return;
//   }

//   const statusCode = err?.status << 0 || 400;
//   const responseData = err?.data || {};

//   // add toast error notification
//   if (
//     notify && // notifications are enabled
//     statusCode !== 404 // is not 404
//   ) {
//     let msg = responseData.message || err.message || defaultMsg;
//     if (msg) {
//       addErrorToast(msg);
//     }
//   }

//   // populate form field errors
//   if (!CommonHelper.isEmpty(responseData.data)) {
//     setErrors(responseData.data);
//   }

//   // unauthorized
//   if (statusCode === 401) {
//     this.cancelAllRequests();
//     return this.logout();
//   }

//   // forbidden
//   if (statusCode === 403) {
//     this.cancelAllRequests();
//     return replace("/");
//   }
// };

const client = {};

client.user = user;
client.transactions = transactions;

export default client;
