import axios from "axios";

const instance = axios.create({
  baseURL: import.meta.env.FT_API_URI,
});

export default instance;
