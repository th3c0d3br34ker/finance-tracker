import { replace } from "svelte-spa-router";
import {
  loadUserFromLocalStorage,
  USER_LOCAL_STORAGE_KEY,
} from "@/stores/user";

import instance from "./axiosInstance";

const user = loadUserFromLocalStorage(USER_LOCAL_STORAGE_KEY);

let isValid = !!user.id || false;

function authWithPassword(email, password) {
  return instance
    .post(
      "/user/login",
      {
        email,
        password,
      },
      {
        headers: { "Content-Type": "application/json" },
      }
    )
    .then((response) => response.data)
    .then((d) => {
      if (d.success) {
        isValid = true;
        return d.data;
      } else {
        isValid = false;
        d.message;
      }
    });
}

function logout(redirect = true) {
  if (redirect) {
    replace("/login");
  }
}

export default { authWithPassword, logout, isValid };
