const util = require('util');
const exec = util.promisify(require('child_process').exec);
const fs = require('fs').promises;
const path = require('path');

const buildPath = path.join(__dirname, '..', 'docs', 'build');
const serverPath = path.join(__dirname, '..', 'server', 'docs');

(async () => {
  try {
    // Console log the paths
    console.log(`Build path: ${buildPath}`);
    console.log(`Server path: ${serverPath}`);

    // Delete the serverPath folder if it exists
    console.log(`Deleting previous files: ${serverPath}`);
    await fs.rm(serverPath, { recursive: true, force: true });
    console.log('Deleted.');

    // Run the build command in the docs folder
    console.log('Building...');
    const { stdout, stderr } = await exec('yarn run build', { cwd: path.resolve(__dirname, '..', 'docs') });
    console.log(`Build stdout: ${stdout}`);
    stderr && console.log(`Build stderr: ${stderr}`);

    // Copy the build folder to the server folder
    console.log(`Copying build to ${serverPath}...`);
    await fs.cp(buildPath, serverPath, { preserveTimestamps: true, recursive: true });
    console.log('Done.');

    // Delete the build folder
    console.log(`Deleting build folder: ${buildPath}`);
    await fs.rm(buildPath, { recursive: true, force: true });
    console.log('Deleted.');
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
})();
