const concurrently = require('concurrently');
const path = require('path');

return concurrently(
  [
    'npm:start-*',
    {
      command: 'nodemon src/index.ts --ignore src/scripts/',
      name: 'server',
      cwd: path.resolve(__dirname, 'server'),
    },
    {
      command: 'vite',
      name: 'frontend',
      cwd: path.resolve(__dirname, 'frontend'),
    },
  ],
  {
    prefix: 'name',
    killOthers: ['failure', 'success'],
    restartTries: 3,
    cwd: path.resolve(__dirname, 'scripts'),
  }
)
  .then((result) => console.log(result))
  .catch((err) => console.error(err));
