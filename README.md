# 💸 finance-tracker

A simple app to track your finances.

<!-- Getting started -->

## 🚀 Getting Started

### 📋 Prerequisites

- Node.js v12.16.1
- yarn v1.22.0 (preferred)
- npm v6.13.4

### 📦 Installing

- Clone the repo

#### 📦 Backend

- `cd` into the `server` directory
- Run `yarn install` to install dependencies
- Run `yarn dev` to start the app

#### 📦 Frontend

- `cd` into the `app` directory
- Run `yarn install` to install dependencies
- Run `yarn start` to start the app

#### 📦 Docs

- `cd` into the `docs` directory
- Run `yarn install` to install dependencies
- Run `yarn start` to start the app

## 📦 Built With

- [node.js](https://nodejs.org/en/) - JavaScript runtime
- [svelte](https://svelte.dev/) - Frontend framework
- [sqlite3](https://www.sqlite.org/index.html) - Database
- [squelize](https://sequelize.org/) - ORM

<!-- Features -->

## 📋 Features

- [x] Add transactions
- [x] Delete transactions
- [x] Edit transactions
- [x] View transactions
- [x] View balance
- [x] View income
- [x] View expenses
- [x] View accounts
- [x] View transactions by date
- [x] View transactions by category
- [x] View transactions by type
- [x] View transactions by account

<!-- License -->

## 📝 License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
