const links = [
  {
    title: 'Basics',
    items: [
      {
        href: '/docs',
        title: 'Introduction'
      }
    ]
  },
  {
    title: 'Web APIs',
    items: [
      {
        href: '/docs/api-accounts',
        title: 'Accounts'
      },
      {
        href: '/docs/api-categories',
        title: 'Categories'
      }
      // {
      //   href: '/docs/api-transactions',
      //   title: 'Transactions'
      // },
      // {
      //   href: '/docs/api-users',
      //   title: 'Users'
      // },
      // {
      //   href: '/docs/api-auth',
      //   title: 'Authentication'
      // },
      // {
      //   href: '/docs/api-settings',
      //   title: 'Settings'
      // },
      // {
      //   href: '/docs/api-health',
      //   title: 'Health'
      // }
    ]
  }
];

export default links;
