import http from 'http';
import { IS_TEST, SYNC_DB, PORT, SERVER_CONTEXT, DATABASE_CONTEXT } from './config';
import logger from './logger';
import app from './routes';
import sequelizeDb from './db';

const server = http.createServer(app);

process.on('uncaughtException', (err) => {
  logger.error(SERVER_CONTEXT, 'uncaughtException', err);
  stopProcess(true);
});

process.on('unhandledRejection', (err) => {
  logger.error(SERVER_CONTEXT, 'unhandledRejection', err);
  stopProcess(true);
});

process.on('SIGINT', () => {
  logger.info(SERVER_CONTEXT, 'SIGINT');
  stopProcess(false);
});

process.on('SIGTERM', () => {
  logger.info(SERVER_CONTEXT, 'SIGTERM');
  stopProcess(false);
});

async function startProcess() {
  if (SYNC_DB) {
    logger.debug(DATABASE_CONTEXT, 'Syncing Database...');
    await sequelizeDb.sync(true);
    logger.debug(DATABASE_CONTEXT, 'Syncing Database complete!');
  } else {
    logger.debug(DATABASE_CONTEXT, 'Skipped syncing Database!');
  }

  server.listen(PORT, () => {
    logger.info(SERVER_CONTEXT, `🚀 Server is listening on http://localhost:${PORT}.`);
  });
}

let stopped = false;

async function stopProcess(err = false) {
  if (stopped) {
    return;
  }
  stopped = true;
  logger.info(SERVER_CONTEXT, 'Stopping server...');
  await server.close();
  process.exit(err ? 1 : 0);
}

if (!IS_TEST) {
  startProcess();
}
