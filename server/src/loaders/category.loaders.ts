/* Importing the controllers and services and then creating instances of them. */
import {
  AddCategoryController,
  FindCategoryByIdController,
  FindAllCategoriesController,
  UpdateCategoryController,
  DeleteCategoryController,
  FindCategoryBySlugController,
} from '../controllers/category.controller';

import sequelizeDb from '../db';
import { CATEGORIES_MODEL_NAME } from '../db/constants';

import CategoryService from '../services/category.service';

export const categoryModel = sequelizeDb.getModel(CATEGORIES_MODEL_NAME);

export const categoryService = new CategoryService(categoryModel);

/* Creating instances of the controllers and services. */
export const addCategory = new AddCategoryController(categoryService);

export const findCategoryById = new FindCategoryByIdController(categoryService);
export const findAllCategories = new FindAllCategoriesController(categoryService);
export const findCategoryBySlug = new FindCategoryBySlugController(categoryService);

export const updateCategory = new UpdateCategoryController(categoryService);

export const deleteCategory = new DeleteCategoryController(categoryService);
