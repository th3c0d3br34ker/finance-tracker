import sequelizeDb from '../db';
import { SESSION_MODEL_NAME } from '../db/constants';
import { SessionService } from '../services/session.service';
import { UserSessionService } from '../services/user-session.service';

const sessionModel = sequelizeDb.getModel(SESSION_MODEL_NAME);

export const sessionService = new SessionService(sessionModel);
export const userSessionService = new UserSessionService(sessionService);
