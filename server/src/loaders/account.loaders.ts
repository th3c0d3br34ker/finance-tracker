import {
  AddAccountController,
  FindAccountByIdController,
  FindAllAccountsController,
  UpdateAccountController,
  DeleteAccountController,
} from '../controllers/account.controllers';
import sequelizeDb from '../db';

import { ACCOUNTS_MODEL_NAME, CREDIT_ACCOUNTS_MODEL_NAME } from '../db/constants';
import AccountService from '../services/account.service';

export const accountModel = sequelizeDb.getModel(ACCOUNTS_MODEL_NAME);
export const creditAccountModel = sequelizeDb.getModel(CREDIT_ACCOUNTS_MODEL_NAME);

export const accountService = new AccountService(accountModel, creditAccountModel);

export const createAccount = new AddAccountController(accountService);

export const findAccountbyId = new FindAccountByIdController(accountService);
export const findAllAccounts = new FindAllAccountsController(accountService);

export const updateAccount = new UpdateAccountController(accountService);

export const deleteAccount = new DeleteAccountController(accountService);
