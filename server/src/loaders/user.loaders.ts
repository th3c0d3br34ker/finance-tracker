import {
  CreateUserController,
  DeleteUserController,
  GetUserController,
  PatchUserController,
  UserOrderByController,
  FindAllUserController,
  UserLoginController,
  UserLogoutController,
  MeUserController,
  UserResetPasswordController,
} from '../controllers/user.controllers';
import Role from '../core/roles';
import sequelizeDb from '../db';
import { ModelNames } from '../db/constants';
import {
  AllowRoleOnlyMiddleware,
  NoSessionMiddleware,
  OnlyLoggedInMiddleware,
  UserSessionMiddleWare,
} from '../middlewares/AuthMiddleWare';
import UserService from '../services/user.service';
import { userSessionService } from './session.loaders';

const userModel = sequelizeDb.getModel(ModelNames.USER);
export const userService = new UserService(userModel);

export const getUsers = new FindAllUserController(userService);
export const getUserById = new GetUserController(userService);
export const deleteUser = new DeleteUserController(userService);
export const updateUser = new PatchUserController(userService);
export const orderBy = new UserOrderByController(userService);
export const createUser = new CreateUserController(userService);
export const login = new UserLoginController(userService, userSessionService);
export const logout = new UserLogoutController(userSessionService);
export const getMeUser = new MeUserController(userService);
export const resetPassword = new UserResetPasswordController(userService);

export const userSessionMiddleware = new UserSessionMiddleWare(userSessionService);
export const superAdminOnlyMiddleware = new AllowRoleOnlyMiddleware([Role.SUPER_ADMIN]);
export const noSessionMiddleware = new NoSessionMiddleware();
export const onlyLoggedInMiddleware = new OnlyLoggedInMiddleware();
