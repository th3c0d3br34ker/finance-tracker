import { AddAssetController, FindAllAssetsController } from '../controllers/asset.controllers';
import sequelizeDb from '../db';
import { ASSET_MODEL_NAME } from '../db/constants';
import AssetService from '../services/asset.service';

export const assetModel = sequelizeDb.getModel(ASSET_MODEL_NAME);

export const assetService = new AssetService(assetModel);

export const createAsset = new AddAssetController(assetService);

export const findAllAssets = new FindAllAssetsController(assetService);
