/* Importing the controllers and services and then creating instances of them. */
import {
  AddTransactionController,
  DeleteTransactionController,
  FindAllTransactionsController,
  FindTransactionByIdController,
  UpdateTransactionController,
} from '../controllers/transaction.controller';

import sequelizeDb from '../db';
import { ModelNames } from '../db/constants';

import TransactionService from '../services/transaction.service';

// required services
import { accountService } from './account.loaders';
import { categoryService } from './category.loaders';

export const transactionModel = sequelizeDb.getModel(ModelNames.TRANSACTIONS);

export const transactionService = new TransactionService(transactionModel, categoryService, accountService);

/* Creating instances of the controllers and services. */
export const addTransaction = new AddTransactionController(transactionService, accountService);

export const findTransactionById = new FindTransactionByIdController(transactionService);
export const findAllTransactions = new FindAllTransactionsController(transactionService);

export const updateTransaction = new UpdateTransactionController(transactionService, accountService);

export const deleteTransaction = new DeleteTransactionController(transactionService);
