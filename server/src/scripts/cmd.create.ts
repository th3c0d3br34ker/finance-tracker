import { exit } from 'process';
import logger from '../logger';
import { client, CLIENT_CONTEXT, getRandomNumber } from './util';

export async function createAccount(user: any) {
  // Generate a random account type
  const accountTypes = ['bank', 'cash', 'wallet'];
  const accountType = accountTypes[getRandomNumber(0, accountTypes.length - 1)];

  // Create a new account with the generated account type
  const { data: account } = await client.POST('/account/new', {
    name: 'Account ' + getRandomNumber(1, 5),
    type: accountType,
    user: user.id,
    accountNumber: '098765432' + getRandomNumber(0, 9),
    openingBalance: 10000,
  });
  logger.info(CLIENT_CONTEXT, `Created account with id: ${account.id} and type ${account.type}.`);

  // Create a new account with the generated account type
  const { data: credit_account } = await client.POST('/account/new', {
    name: 'Credit Card',
    type: 'credit-card',
    user: user.id,
    accountNumber: '098765432' + getRandomNumber(0, 9),
    creditLimit: 20000,
  });
  logger.info(CLIENT_CONTEXT, `Created account with id: ${credit_account.id} and type ${credit_account.type}.`);

  return account;
}

export async function createCategories() {
  const categoriesData = [
    { type: 'income', name: 'Salary/Wages' },
    { type: 'income', name: 'Bonus' },
    { type: 'income', name: 'Tips/Gratuities' },
    { type: 'income', name: 'Investment Income' },
    { type: 'income', name: 'Rental Income' },
    { type: 'income', name: 'Side Hustle Income' },
    { type: 'income', name: 'Gift Income' },
    { type: 'income', name: 'Alimony/Child Support' },
    { type: 'expense', name: 'Rent/Mortgage' },
    { type: 'expense', name: 'Utilities', subCategories: ['Electricity', 'Water', 'Gas'] },
    { type: 'expense', name: 'Food/Groceries' },
    { type: 'expense', name: 'Transportation', subCategories: ['Gas', 'Car Payment', 'Public Transportation'] },
    { type: 'expense', name: 'Insurance', subCategories: ['Car', 'Homeowners / Renters', 'Health'] },
    { type: 'expense', name: 'Healthcare', subCategories: ['Doctor Visits', 'Prescriptions', 'Medical Procedures'] },
    { type: 'expense', name: 'Entertainment', subCategories: ['Movies', 'Music', 'Books', 'Dining Out'] },
    { type: 'expense', name: 'Personal Care', subCategories: ['Haircuts', 'Makeup', 'Toiletries'] },
    { type: 'expense', name: 'Education', subCategories: ['Tuition', 'Books', 'Supplies'] },
    { type: 'expense', name: 'Charitable Donations' },
    { type: 'expense', name: 'Savings/Investments' },
  ];

  const categories: any = [];

  for (const eachCategory of categoriesData) {
    try {
      const { data: category } = await client.POST('/category/new', {
        type: eachCategory.type,
        name: eachCategory.name,
      });

      logger.info(
        CLIENT_CONTEXT,
        `Created category with id: ${category.id}, name: ${category.name} and type: ${category.type}.`,
      );

      if (eachCategory.subCategories) {
        for (const eachSubCategoryName of eachCategory.subCategories) {
          const { data: sub_category } = await client.POST('/category/new', {
            type: eachCategory.type,
            name: eachSubCategoryName,
            parent: category.id,
          });

          logger.info(
            CLIENT_CONTEXT,
            `Created category with id: ${sub_category.id}, name: ${sub_category.name}, type: ${sub_category.type} and parent: ${sub_category.parent}.`,
          );

          categories.push(sub_category);
        }
      }
      categories.push(category);
    } catch (err: any) {
      logger.error(CLIENT_CONTEXT, `Failed to create category ${eachCategory.name}: ${(err as Error).message}`);
      exit(1);
    }
  }

  return categories;
}

export async function createTransactions(account: any, categories: any[]) {
  // Generate and create 5 random transactions with the generated account id and type
  for (let i = 0; i < 5; i++) {
    const transactionType = Math.random() < 0.5 ? 'income' : 'expense';
    const amount = getRandomNumber(100, 1000);
    const category = categories[getRandomNumber(0, categories.length - 1)];

    try {
      const { data: transaction } = await client.POST('/transaction/new', {
        type: transactionType,
        account: account.id,
        amount: amount,
        description: 'test',
        category: category.id,
      });

      logger.info(
        CLIENT_CONTEXT,
        `Created ${transactionType} transaction with id ${transaction.id}, amount ${amount}, and category ${category.name}.`,
      );
    } catch (err) {
      logger.error(
        CLIENT_CONTEXT,
        `Failed to create transaciton with ${transactionType} transaction with amount ${amount}, and category ${category.name}.`,
      );
      exit(1);
    }
  }
}

export async function createUser() {
  // Create a new account with the generated account type
  const { data: user } = await client.POST('/user/new', {
    name: 'User',
    email: 'random@example.com',
    password: 'thisisanawesomepassword',
  });

  logger.info(CLIENT_CONTEXT, `Created user with id: ${user.id}.`);

  return user;
}
