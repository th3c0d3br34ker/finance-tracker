import { createAccount, createCategories, createTransactions, createUser } from './cmd.create';
import { fetchAccount, fetchCategories } from './cmd.fetch';

async function driver() {
  const create = process.argv.includes('--create');

  if (create) {
    const user = await createUser();
    await createAccount(user);
    await createCategories();
  }
  const account = await fetchAccount();
  const categories = await fetchCategories();

  createTransactions(account, categories);
}

driver();
