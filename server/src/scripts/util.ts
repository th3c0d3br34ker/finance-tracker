import fetch from 'node-fetch';
import logger from '../logger';

const API_URL = 'http://localhost:3050';

export const CLIENT_CONTEXT = 'CLIENT';

// Generate a random number between min and max (inclusive)
export function getRandomNumber(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function GET(route: string, body?: any) {
  const response = await fetch(`${API_URL}${route}`, {
    method: 'GET',
    body: JSON.stringify(body),
  });

  if (response.ok) {
    const json = await response.json();
    return json;
  }

  logger.error(CLIENT_CONTEXT, `Request failed with status ${response.status}`);
}

async function PATCH(route: string, body?: any) {
  const response = await fetch(`${API_URL}${route}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PATCH',
    body: JSON.stringify(body),
  });

  if (response.ok) {
    const json = await response.json();
    return json;
  }

  logger.error(CLIENT_CONTEXT, `Request failed with status ${response.status}`);
}

async function POST(route: string, body?: any) {
  const response = await fetch(`${API_URL}${route}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify(body),
  });

  if (response.ok) {
    const json = await response.json();
    return json;
  }

  logger.error(CLIENT_CONTEXT, `Request failed with status ${response.status}`);
}

async function DELETE(route: string) {
  const response = await fetch(`${API_URL}${route}`, {
    method: 'DELETE',
  });

  if (response.ok) {
    const json = await response.json();
    return json;
  }

  logger.error(CLIENT_CONTEXT, `Request failed with status ${response.status}`);
}

export const client = { GET, PATCH, POST, DELETE };
