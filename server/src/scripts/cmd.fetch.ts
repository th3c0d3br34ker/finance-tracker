import { client, getRandomNumber } from './util';

export async function fetchAccount() {
  const { data } = await client.GET('/account');

  return data.accounts[getRandomNumber(0, data.totalItems - 1)];
}

export async function fetchCategories() {
  const { data } = await client.GET('/category');

  return data.categories;
}

export async function fetchUser() {
  const { data } = await client.GET('/user');

  return data.users[getRandomNumber(0, data.totalItems - 1)];
}
