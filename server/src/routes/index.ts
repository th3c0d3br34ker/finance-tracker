import cors from 'cors';
import express from 'express';
import cookieParser from 'cookie-parser';
import NotFoundHandler from '../controllers/notFoundHandler';
import GlobalErrorHandler from '../controllers/globalErrorHandler';

import UserRoute from './user';
import AccountRoute from './account';
import TransactionRoute from './transaction';
import CategoryRoute from './category';

import requestLogger from '../middlewares/requestLogger';
import { NODE_ENV } from '../config';

const app = express();

app.disable('x-powered-by');
app.enable('case sensitive routing');
app.disable('strict routing');
app.enable('trust proxy');
app.set('env', NODE_ENV);

app.use(
  cors((_req: any, cb: (arg0: null, arg1: { origin: boolean; credentials: boolean }) => void) => {
    cb(null, {
      origin: true,
      credentials: true,
    });
  }),
);
app.use(express.json());
app.use(cookieParser());

// Add logger
app.use(requestLogger);

// Static Routes
const docs = express.static('docs');
const frontendApp = express.static('app');

// Application Specific Routes
app.use('/', docs);
app.use('/app', frontendApp);
app.use('/user', UserRoute);
app.use('/account', AccountRoute);
app.use('/transaction', TransactionRoute);
app.use('/category', CategoryRoute);

// 404 error
app.use(NotFoundHandler);

// global error handler
app.use(GlobalErrorHandler);

export default app;
