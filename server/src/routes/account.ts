import { NextFunction, Router, Request, Response } from 'express';
import validator from 'validator';

import { BadRequestError } from '../errors/api.error';
import {
  createAccount,
  findAllAccounts,
  findAccountbyId,
  updateAccount,
  deleteAccount,
} from '../loaders/account.loaders';

const route = Router();

route.param('accountId', (_req: Request, _res: Response, next: NextFunction, id) => {
  if (!validator.isUUID(id, 4)) {
    next(new BadRequestError('Invalid Account Id'));
    return;
  }
  next();
});

route.get('/', findAllAccounts.execute);
route.post('/new', createAccount.execute);
route.get('/:accountId', findAccountbyId.execute);
route.patch('/:accountId', updateAccount.execute);
route.delete('/:accountId', deleteAccount.execute);

export default route;
