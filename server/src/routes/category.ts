import { NextFunction, Router, Request, Response } from 'express';
import { validate } from 'uuid';

import { BadRequestError } from '../errors/api.error';
import {
  addCategory,
  findCategoryById,
  findCategoryBySlug,
  findAllCategories,
  updateCategory,
  deleteCategory,
} from '../loaders/category.loaders';

const route = Router();

route.param('categoryId', (_req: Request, _res: Response, next: NextFunction, id) => {
  if (!validate(id)) {
    next(new BadRequestError('Invalid Category Id'));
    return;
  }
  next();
});

// route.use(userSessionMiddleware.apply, onlyLoggedInMiddleware.apply);

route.get('/', findAllCategories.execute);
route.post('/new', addCategory.execute);
route.get('/slug/:slug', findCategoryBySlug.execute);
route.get('/:categoryId', findCategoryById.execute);
route.patch('/:categoryId', updateCategory.execute);
route.delete('/:categoryId', deleteCategory.execute);

export default route;
