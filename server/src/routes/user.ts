import { NextFunction, Router, Request, Response } from 'express';
import validator from 'validator';
import { BadRequestError } from '../errors/api.error';
import {
  deleteUser,
  getUserById,
  orderBy,
  updateUser,
  createUser,
  getUsers,
  login,
  logout,
  getMeUser,
  userSessionMiddleware,
  noSessionMiddleware,
  resetPassword,
} from '../loaders/user.loaders';

const route = Router();

route.param('id', (_req: Request, _res: Response, next: NextFunction, id) => {
  if (!validator.isUUID(id, 4)) {
    next(new BadRequestError('Invalid User Id'));
    return;
  }
  next();
});

route.post('/new', createUser.execute);
route.delete('/:id', userSessionMiddleware.apply, deleteUser.execute);
route.patch('/:id', userSessionMiddleware.apply, updateUser.execute);

route.post('/reset/password', userSessionMiddleware.apply, resetPassword.execute);
route.post('/reset/email');
route.post('/login', noSessionMiddleware.apply, login.execute);
route.get('/logout', logout.execute);

route.get('/orderBy', userSessionMiddleware.apply, orderBy.execute);
route.get('/all', userSessionMiddleware.apply, getUsers.execute);

route.get('/me', userSessionMiddleware.apply, getMeUser.execute);
route.get('/:id', userSessionMiddleware.apply, getUserById.execute);

export default route;
