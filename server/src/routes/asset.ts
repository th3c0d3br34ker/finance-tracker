import { NextFunction, Request, Response, Router } from 'express';
import { validate } from 'uuid';

import { BadRequestError } from '../errors/api.error';
import { createAsset, findAllAssets } from '../loaders/asset.loader';

const route = Router();

route.param('assetId', (_req: Request, _res: Response, next: NextFunction, id) => {
  if (!validate(id)) {
    next(new BadRequestError('Invalid Asset Id'));
    return;
  }
  next();
});

route.get('/', findAllAssets.execute);
route.post('/new', createAsset.execute);
// route.get('/:categoryId', findCategoryById.execute);
// route.patch('/:categoryId', updateCategory.execute);
// route.delete('/:categoryId', deleteCategory.execute);

export default route;
