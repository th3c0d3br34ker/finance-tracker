import { NextFunction, Router, Request, Response } from 'express';
import validator from 'validator';

import { BadRequestError } from '../errors/api.error';
import {
  findAllTransactions,
  addTransaction,
  deleteTransaction,
  updateTransaction,
  findTransactionById,
} from '../loaders/transaction.loaders';

const route = Router();

route.param('transactionId', (_req: Request, _res: Response, next: NextFunction, id) => {
  if (!validator.isUUID(id, 4)) {
    next(new BadRequestError('Invalid Transaction Id'));
    return;
  }
  next();
});

// route.use(userSessionMiddleware.apply, onlyLoggedInMiddleware.apply);

route.get('/', findAllTransactions.execute);
route.post('/new', addTransaction.execute);
route.delete('/:transactionId', deleteTransaction.execute);
route.patch('/:transactionId', updateTransaction.execute);
route.get('/:transactionId', findTransactionById.execute);

export default route;
