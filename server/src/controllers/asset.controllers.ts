import { Request, Response } from 'express';

import { BaseController } from './BaseController';
import { IAssetService } from '../services/asset.service';
import logger from '../logger';

export class AddAssetController extends BaseController {
  private _assetService: IAssetService;

  constructor(assetService: IAssetService) {
    super();
    this._assetService = assetService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { name, price, quantity, symbol, type } = req.body;

      const asset = this._assetService.add({
        name,
        price,
        quantity,
        symbol,
        type,
      });

      BaseController.created(res, (await asset).toJSON(), `Asset created: ${type}.`);
    } catch (err) {
      logger.error('add-asset-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindAssetByIdController extends BaseController {
  protected async executeImpl(): Promise<void> {
    throw new Error('Method not implemented.');
  }
}

export class FindAllAssetsController extends BaseController {
  private _assetService: IAssetService;

  constructor(assetService: IAssetService) {
    super();
    this._assetService = assetService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { currentPage, pageSize } = req.query;
      const filters = {
        pageSize,
        page: currentPage,
      };

      const { assets, totalPage, currentPage: cpage } = await this._assetService.find(filters);
      BaseController.ok(
        res,
        {
          assets,
          totalItems: assets.length,
          currentPage: cpage,
          totalPage,
        },
        'Assets fetched successfully.',
      );
    } catch (err) {
      logger.error('find-asset-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UpdateAssetController extends BaseController {
  protected async executeImpl(): Promise<void> {
    throw new Error('Method not implemented.');
  }
}

export class DeleteAssetController extends BaseController {
  protected async executeImpl(): Promise<void> {
    throw new Error('Method not implemented.');
  }
}

export class FindAllAssetsByPortfolioController extends BaseController {
  protected async executeImpl(): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
