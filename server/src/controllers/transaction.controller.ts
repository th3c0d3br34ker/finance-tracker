import { Request, Response } from 'express';

import logger from '../logger';
import { IAccountService } from '../services/account.service';
import { ITransactionService } from '../services/transaction.service';
import { TransactionType } from '../core/transaction-types';

import { BaseController } from './BaseController';

export class AddTransactionController extends BaseController {
  private readonly _transactionService: ITransactionService;
  private readonly _accountService: IAccountService;

  constructor(transactionService: ITransactionService, accountService: IAccountService) {
    super();
    this._transactionService = transactionService;
    this._accountService = accountService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { type, account, amount, description, recurring, category } = req.body;

      const transaction = await this._transactionService.add({
        type,
        account,
        amount,
        category,
        description,
        recurring,
      });

      const currentAccount = await this._accountService.findOne(account);

      // Calculating the current balance of the account based on the previous balance and the updated transaction details.
      const currentBalance =
        currentAccount.balance + Number.parseFloat(type === TransactionType.INCOME ? amount : -amount);

      // Update the current balance of the account in the database.
      await this._accountService.update(account, { balance: currentBalance });

      BaseController.created(res, transaction.toJSON(), `Transaction created: ${transaction.id}`);
    } catch (err) {
      logger.error('add-transaction-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindTransactionByIdController extends BaseController {
  private readonly _transactionService: ITransactionService;

  constructor(transactionService: ITransactionService) {
    super();
    this._transactionService = transactionService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { transactionId } = req.params;

      const data = await this._transactionService.findOne(transactionId);

      BaseController.ok(res, data, `Transactions found: ${transactionId}`);
    } catch (err) {
      logger.error('find-transaction-by-id-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindAllTransactionsController extends BaseController {
  private readonly _transactionService: ITransactionService;

  constructor(transactionService: ITransactionService) {
    super();
    this._transactionService = transactionService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { currentPage, pageSize } = req.query;
      const filters = {
        pageSize,
        page: currentPage,
      };

      const { transactions, totalPage, currentPage: cpage } = await this._transactionService.find(filters);

      BaseController.ok(res, {
        transactions,
        totalItems: transactions.length,
        currentPage: cpage,
        totalPage: totalPage,
      });
    } catch (err) {
      logger.error('find-all-transactions-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UpdateTransactionController extends BaseController {
  private readonly _transactionService: ITransactionService;
  private readonly _accountService: IAccountService;

  constructor(transactionService: ITransactionService, accountService: IAccountService) {
    super();
    this._transactionService = transactionService;
    this._accountService = accountService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { transactionId } = req.params;
      const { type, account, amount, description, recurring } = req.body;

      // Retrive the old transaction
      const oldTransaction = await this._transactionService.findOne(transactionId);

      // Retrieve the account associated with the updated transaction.
      const updatedAccount = await this._accountService.findOne(account);

      // Calculate the current balance of the account based on the previous balance and the updated transaction details.
      const previousBalance =
        updatedAccount.balance + (type === TransactionType.INCOME ? oldTransaction.amount : -oldTransaction.amount);
      const currentBalance = previousBalance + Number.parseFloat(type === TransactionType.INCOME ? amount : -amount);

      const transaction = await this._transactionService.update(transactionId, {
        type,
        account,
        amount,
        description,
        recurring,
      });

      // Update the current balance of the account in the database.
      await this._accountService.update(account, { balance: currentBalance });

      BaseController.ok(res, transaction.toJSON(), `Transaction updated: ${transactionId}`);
    } catch (err) {
      logger.error('update-transaction-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class DeleteTransactionController extends BaseController {
  private readonly _transactionService: ITransactionService;

  constructor(transactionService: ITransactionService) {
    super();
    this._transactionService = transactionService;
  }

  protected async executeImpl(req: Request, res: Response) {
    try {
      const { transactionId } = req.params;

      const transaction = await this._transactionService.delete(transactionId);

      BaseController.ok(res, transaction, `Transaction deleted: ${transaction.id}`);
    } catch (err) {
      logger.error('delete-transaction-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}
