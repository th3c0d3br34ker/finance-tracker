import { Request, Response } from 'express';
import logger from '../logger';
import CategoryService from '../services/category.service';
import { BaseController } from './BaseController';

export class AddCategoryController extends BaseController {
  private readonly _categoryService: CategoryService;

  constructor(categoryService: CategoryService) {
    super();
    this._categoryService = categoryService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { name, type, color, slug, description, parent } = req.body;
      const category = await this._categoryService.add({
        name,
        type,
        color,
        slug,
        description,
        parent,
      });

      BaseController.ok(res, category.toJSON(), 'Category created.');
    } catch (err) {
      logger.error('add-category-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindAllCategoriesController extends BaseController {
  private readonly _categoryService: CategoryService;

  constructor(categoryService: CategoryService) {
    super();
    this._categoryService = categoryService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { currentPage, pageSize, parent } = req.query;
      const filters = {
        pageSize,
        page: currentPage,
        parent,
      };
      const { data, totalPage, currentPage: cpage } = await this._categoryService.find(filters);

      BaseController.ok(res, {
        categories: data,
        totalItems: data.length,
        currentPage: cpage,
        totalPage: totalPage,
      });
    } catch (err) {
      logger.error('find-all-categories-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindCategoryByIdController extends BaseController {
  private readonly _categoryService: CategoryService;

  constructor(categoryService: CategoryService) {
    super();
    this._categoryService = categoryService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { categoryId } = req.params;
      const category = await this._categoryService.findOne(categoryId);

      BaseController.ok(res, category.toJSON(), `Category found: ${categoryId}`);
    } catch (err) {
      logger.error('add-category-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindCategoryBySlugController extends BaseController {
  private readonly _categoryService: CategoryService;

  constructor(categoryService: CategoryService) {
    super();
    this._categoryService = categoryService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { slug } = req.params;
      const category = await this._categoryService.findOneBySlug(slug);

      BaseController.ok(res, category.toJSON(), `Category found: ${slug}`);
    } catch (err) {
      logger.error('add-category-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UpdateCategoryController extends BaseController {
  private readonly _categoryService: CategoryService;

  constructor(categoryService: CategoryService) {
    super();
    this._categoryService = categoryService;
  }
  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { categoryId } = req.params;
      const { name, color, description } = req.body;

      const category = await this._categoryService.update(categoryId, {
        name,
        color,
        description,
      });

      BaseController.ok(res, category.toJSON(), `Category updated: ${categoryId}`);
    } catch (err) {
      logger.error('add-category-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class DeleteCategoryController extends BaseController {
  private readonly _categoryService: CategoryService;

  constructor(categoryService: CategoryService) {
    super();
    this._categoryService = categoryService;
  }
  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { categoryId } = req.params;

      const category = await this._categoryService.delete(categoryId);

      BaseController.ok(res, category.toJSON(), `Category deleted: ${categoryId}`);
    } catch (err) {
      logger.error('add-category-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}
