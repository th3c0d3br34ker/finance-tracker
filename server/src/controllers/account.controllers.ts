import { Request, Response } from 'express';

import logger from '../logger';
import { IAccountService } from '../services/account.service';
import { BaseController } from './BaseController';

export class AddAccountController extends BaseController {
  private _accountsService: IAccountService;

  constructor(accountsService: IAccountService) {
    super();
    this._accountsService = accountsService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { name, type, user, accountNumber, openingBalance, creditLimit } = req.body;

      const account = this._accountsService.add({
        name,
        type,
        user,
        accountNumber,
        openingBalance,
        creditLimit,
      });

      BaseController.created(res, (await account).toJSON(), `Account created: ${type}.`);
    } catch (err) {
      logger.error('add-account-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindAccountByIdController extends BaseController {
  private _accountService: IAccountService;

  constructor(accountService: IAccountService) {
    super();
    this._accountService = accountService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { accountId } = req.params;

      const account = this._accountService.findOne(accountId);

      BaseController.ok(res, (await account).toJSON(), `Account found: ${accountId}.`);
    } catch (err) {
      logger.error('find-account-by-id-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindAllAccountsController extends BaseController {
  private _accountService: IAccountService;

  constructor(accountService: IAccountService) {
    super();
    this._accountService = accountService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { currentPage, pageSize } = req.query;
      const filters = {
        pageSize,
        page: currentPage,
      };

      const { accounts, totalPage, currentPage: cpage } = await this._accountService.find(filters);
      BaseController.ok(
        res,
        {
          accounts,
          totalItems: accounts.length,
          currentPage: cpage,
          totalPage: totalPage,
        },
        'Accounts fetched successfully.',
      );
    } catch (err) {
      logger.error('find-all-accounts-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UpdateAccountController extends BaseController {
  private _accountService: IAccountService;

  constructor(accountService: IAccountService) {
    super();
    this._accountService = accountService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { accountId } = req.params;
      const { name, type, accountNumber, openingBalance } = req.body;

      if (openingBalance) {
        BaseController.forbidden(res, 'Updating Opening Balance not allowed.');
        return;
      }

      const account = this._accountService.update(accountId, {
        name,
        type,
        accountNumber,
      });

      BaseController.ok(res, (await account).toJSON(), `Account: ${accountId} updated!`);
    } catch (err) {
      logger.error('update-account-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class DeleteAccountController extends BaseController {
  private _accountService: IAccountService;

  constructor(accountService: IAccountService) {
    super();
    this._accountService = accountService;
  }

  protected async executeImpl(req: Request, res: Response) {
    try {
      const { accountId } = req.params;
      const { allowDelete } = req.query;

      if (allowDelete) {
        let account = await this._accountService.findOne(accountId);

        account = await this._accountService.delete(accountId);

        BaseController.ok(res, account.toJSON(), `Account deleted: ${accountId}`);
        return;
      }

      BaseController.forbidden(res, `Account: ${accountId} cannot be deleted.`);
    } catch (err) {
      logger.error('delete-account-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}
