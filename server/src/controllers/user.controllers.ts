import { CookieOptions, Request, Response } from 'express';
import _ from 'lodash';
import logger from '../logger';
import {
  IUserSessionService,
  USER_SESSION_MAXAGE,
  USER_SESSION_NAME,
  USER_SESSION_SIGN,
} from '../services/user-session.service';
import { IUserService } from '../services/user.service';
import { BaseController } from './BaseController';
import * as config from '../config';
import { NotFoundError } from '../errors/api.error';

const cookieDeleteOptions: CookieOptions = {
  maxAge: 0,
  httpOnly: true,
  secure: config.NODE_ENV === 'production',
  sameSite: 'none',
  signed: false,
};

export class CreateUserController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  public async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { name, email, password } = req.body;
      const user = await this._userService.create({ name, email, password });
      BaseController.created(res, user.toJSON());
    } catch (err: any) {
      logger.error('create-user-controller', err);
      BaseController.fail(res, err);
    }
  }
}

export class PatchUserController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  public async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params;
      const user = await this._userService.update(id, req.body);
      BaseController.ok(res, user.toJSON());
    } catch (err: any) {
      logger.error('update-user-controller', err);
      BaseController.fail(res, err);
    }
  }
}

export class DeleteUserController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  public async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params;
      const user = await this._userService.delete(id);
      BaseController.ok(res, user.toJSON());
    } catch (err: any) {
      logger.error('delete-user-controller', err);
      BaseController.fail(res, err);
    }
  }
}

export class GetUserController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  public async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params;
      const user = await this._userService.getById(id, false);
      BaseController.ok(res, user.toJSON());
    } catch (err: any) {
      logger.error('get-user-controller', err);
      BaseController.fail(res, err);
    }
  }
}

export class UserOrderByController extends BaseController {
  private _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  protected async executeImpl(_req: Request, res: Response): Promise<void> {
    try {
      BaseController.ok(
        res,
        this._userService.getAllowedOrderByFields().sort((a, b) => a.localeCompare(b)),
      );
    } catch (err) {
      logger.error('user-orderBy-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UserResetEmailController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params;
      const { email } = req.body;
      const user = await this._userService.resetEmail(id, email);
      BaseController.ok(res, user.toJSON());
    } catch (err) {
      logger.error('user-reset-email-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UserResetPasswordController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { password, id } = req.body;
      await this._userService.resetPassword(id, password);
      BaseController.ok(res);
    } catch (err) {
      logger.error('user-reset-email-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UserLoginController extends BaseController {
  private readonly _userService: IUserService;
  private readonly _sessionService: IUserSessionService;

  constructor(userService: IUserService, sessionService: IUserSessionService) {
    super();
    this._userService = userService;
    this._sessionService = sessionService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { email, password } = req.body;
      const user = await this._userService.login(email, password);
      const session = await this._sessionService.createSession(user);
      const cookieOptions: CookieOptions = {
        httpOnly: true,
        secure: config.NODE_ENV === 'production',
        maxAge: USER_SESSION_MAXAGE,
        sameSite: 'none',
        signed: false,
      };
      res.cookie(USER_SESSION_NAME, session.id, cookieOptions);
      res.cookie(USER_SESSION_SIGN, session.signature, cookieOptions);
      BaseController.ok(res, user.toJSON());
    } catch (err) {
      logger.error('user-login-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class UserLogoutController extends BaseController {
  private readonly _sessionService: IUserSessionService;

  constructor(sessionService: IUserSessionService) {
    super();
    this._sessionService = sessionService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const session = req.cookies[USER_SESSION_NAME];
      const signature = req.cookies[USER_SESSION_SIGN];
      if (_.isEmpty(session) || _.isEmpty(signature)) {
        BaseController.ok(res);
        return;
      }
      await this._sessionService.destroyUserSession(session, signature);
      res.cookie(USER_SESSION_NAME, '', cookieDeleteOptions);
      res.cookie(USER_SESSION_SIGN, '', cookieDeleteOptions);
      BaseController.ok(res);
    } catch (err) {
      logger.error('user-logout-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class MeUserController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      if (!_.isNil((req as any).user)) {
        const user = await this._userService.getById((req as any).user.id, false);
        BaseController.ok(res, user.toJSON());
      } else {
        res.cookie(USER_SESSION_NAME, '', cookieDeleteOptions);
        res.cookie(USER_SESSION_SIGN, '', cookieDeleteOptions);
        BaseController.fail(res, new NotFoundError('User not found'));
      }
    } catch (err) {
      logger.error('user-me-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}

export class FindAllUserController extends BaseController {
  private readonly _userService: IUserService;

  constructor(userService: IUserService) {
    super();
    this._userService = userService;
  }

  protected async executeImpl(req: Request, res: Response): Promise<void> {
    try {
      const { search, orderBy, page, pageSize } = req.query;

      const {
        data: users,
        totalPage,
        currentPage: cpage,
      } = await this._userService.getAll({
        search,
        orderBy,
        page,
        pageSize,
      });
      BaseController.ok(res, {
        users: users.map((user) => user.toJSON()),
        totalPage,
        currentPage: cpage,
        totalItems: users.length,
      });
    } catch (err) {
      logger.error('find-all-user-controller', err);
      BaseController.fail(res, err as Error);
    }
  }
}
