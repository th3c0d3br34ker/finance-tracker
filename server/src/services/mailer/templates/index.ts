export abstract class BaseMailTemplate {
  private _name: string;
  private _defaultContext: object;

  constructor(templateName: string, context: object = Object.create(null)) {
    this._name = templateName;
    this._defaultContext = context;
  }

  get name(): string {
    return this._name;
  }

  private _makeContext(context: object = {}): object {
    return Object.assign({}, this._defaultContext, context);
  }

  abstract _render(context: object): string;

  public render(context: object = {}): string {
    return this._render(this._makeContext(context));
  }
}

export default BaseMailTemplate;
