import _ from 'lodash';
import nodemailer from 'nodemailer';
import BaseMailTemplate from './templates';
import fs from 'fs';
import logger from '../../logger';

let transportData: any = {
  host: 'smtp.gmail.com',
  port: 465,
  auth: {
    user: '',
    pass: '',
  },
};
try {
  const data = JSON.parse(fs.readFileSync('./transporter.json').toString());
  transportData = Object.assign(transportData, data);
} catch (e) {
  logger.error('mailer service', 'Error reading transporter.json file');
}

class MailerService {
  private readonly _transporter: nodemailer.Transporter;
  private readonly _templates: Map<string, BaseMailTemplate>;

  constructor() {
    this._templates = new Map<string, BaseMailTemplate>();
    this._transporter = nodemailer.createTransport({
      host: transportData.host,
      port: transportData.port,
      auth: transportData.auth,
      socketTimeout: 60000,
      pool: true,
      maxConnections: 3,
      maxMessages: 50,
    });
  }

  public sendMail(to: string, subject: string, html: string): Promise<nodemailer.SentMessageInfo> {
    return this._transporter.sendMail({
      to,
      from: process.env.MAIL_USER,
      subject,
      html,
    });
  }

  public sendMailWithTemplate(
    to: string,
    subject: string,
    template: BaseMailTemplate,
    context: any,
  ): Promise<nodemailer.SentMessageInfo> {
    return this._transporter.sendMail({
      to,
      from: process.env.MAIL_USER,
      subject,
      html: template.render(context),
    });
  }

  public sendTemplatedMail(
    to: string,
    subject: string,
    templateName: string,
    context: any,
  ): Promise<nodemailer.SentMessageInfo> {
    const template = this._templates.get(templateName);
    if (!_.isNil(template)) {
      return this.sendMailWithTemplate(to, subject, template, context);
    } else {
      throw new Error(`Template ${templateName} not found`);
    }
  }

  public addTemplate(template: BaseMailTemplate): void {
    this._templates.set(template.name, template);
  }

  public hasTemplate(templateName: string): boolean {
    return this._templates.has(templateName);
  }
}

const mailerService = new MailerService();

export default mailerService;
