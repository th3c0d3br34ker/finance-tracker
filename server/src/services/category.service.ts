import { v4 as uuidv4, validate } from 'uuid';
import { Model, ModelStatic, Order, WhereOptions } from 'sequelize/types';

import CategoryEntity, { Category } from '../core/entities/category';
import logger from '../logger';
import { retryableDecorator } from '../utils';
import ApiError, { NotFoundError } from '../errors/api.error';
import CoreError from '../core/errors';

export interface CategoryDTO {
  name: Category['name'];
  type: Category['type'];
  color: Category['color'];
  slug: Category['slug'];
  description: Category['description'];
  parent: Category['parent'];
}

export interface ICategoryService {
  add(category: Partial<CategoryDTO>): Promise<CategoryEntity>;

  findOne(categoryId: Category['id']): Promise<CategoryEntity>;
  findOneBySlug(slug: Category['slug']): Promise<CategoryEntity>;
  find(_filters: any): Promise<{
    data: CategoryEntity[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }>;

  update(categoryId: Category['id'], updates: Partial<CategoryDTO>): Promise<CategoryEntity>;
  delete(categoryId: Category['id']): Promise<CategoryEntity>;
}

export default class CategoryService implements ICategoryService {
  private readonly _categoryModel: ModelStatic<Model>;

  constructor(model: ModelStatic<Model>) {
    this._categoryModel = model;
  }

  private _findCategoryModel(id: Category['id'], nonDeleted = false, onlyParent = true): Promise<Model> {
    if (!validate(id)) {
      throw new NotFoundError('Invalid categoryId found!');
    }
    const whereOptions: WhereOptions = { id };

    if (nonDeleted) whereOptions.deletedAt = null;
    if (onlyParent) whereOptions.parent = null;

    return this._categoryModel
      .findOne({
        where: whereOptions,
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("Category doesn't exist!");
        }
        return res;
      });
  }

  async add(category: CategoryDTO): Promise<CategoryEntity> {
    const self = this;

    function tryInsert(_category: Category) {
      const categoryEnitity = CategoryEntity.create({
        ..._category,
        id: uuidv4(),
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      return self._categoryModel.create(categoryEnitity.toPersistant(), {
        isNewRecord: true,
      });
    }

    function errorCheck(err: Error) {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return true;
      }
      return false;
    }

    const createFunc = retryableDecorator(tryInsert, errorCheck, 10);

    return createFunc(category)
      .then((res: any) => {
        return CategoryEntity.create({
          ...res.dataValues,
        } as Category);
      })
      .catch((err) => {
        logger.error((err as Error).message, err);
        if (err instanceof ApiError || err instanceof CoreError) {
          throw err;
        }
        throw new Error('Unable to create new Category.');
      });
  }

  async findOne(categoryId: string): Promise<CategoryEntity> {
    const self = this;

    try {
      const category = await self._findCategoryModel(categoryId, true, false);

      logger.debug('>>>>', category.get(''));

      return CategoryEntity.create(category.toJSON() as Category);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Category.');
    }
  }

  async findOneBySlug(slug: string): Promise<CategoryEntity> {
    const self = this;

    try {
      const where: WhereOptions = {
        slug,
        deletedAt: null,
      };

      const category = await self._categoryModel.findOne({
        where,
      });

      if (category === null) {
        throw new NotFoundError("Category doesn't exist!");
      }

      return CategoryEntity.create(category.toJSON() as Category);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Category.');
    }
  }

  async find(_filters: any): Promise<{
    data: CategoryEntity[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }> {
    const self = this;

    try {
      const filters = _filters || {};
      const limit = filters.pageSize || 10;
      const currentPage = filters.page || 1;
      const offset = (currentPage - 1) * limit;
      const orderBy: Order = [
        ['updatedAt', 'DESC'],
        ['id', 'DESC'],
      ];

      const where: WhereOptions = {
        deletedAt: null,
        parent: filters.parent || null,
      };

      const categories = await self._categoryModel.findAndCountAll({
        where,
        order: orderBy,
        limit,
        offset,
      });

      const categoryEntities = categories.rows.map((category) => {
        return CategoryEntity.create(category.toJSON() as Category);
      });

      return {
        data: categoryEntities,
        totalPage: Math.ceil(categories.count / limit),
        currentPage,
        limit,
      };
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Categories.');
    }
  }

  async update(categoryId: string, updates: Partial<CategoryDTO>): Promise<CategoryEntity> {
    const self = this;

    try {
      let category = await self._findCategoryModel(categoryId, true);

      category = await category.update(updates);
      category = await category.save();

      return CategoryEntity.create(category.toJSON() as Category);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to update Category.');
    }
  }

  async delete(categoryId: string): Promise<CategoryEntity> {
    const self = this;

    try {
      let category = await self._findCategoryModel(categoryId, true);

      category = await category.update({ deletedAt: new Date(Date.now()) });

      return CategoryEntity.create(category.toJSON() as Category);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to delete Category.');
    }
  }
}
