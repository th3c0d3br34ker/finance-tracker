import { v4 as uuidv4 } from 'uuid';
import { Model, ModelStatic, WhereOptions, Order } from 'sequelize/types';

import AssetEntity, { Asset } from '../core/entities/asset';
import ApiError, { NotFoundError } from '../errors/api.error';
import logger from '../logger';
import CoreError from '../core/errors';
import { retryableDecorator } from '../utils';

export interface AssetDTO {
  name: Asset['name'];
  symbol: Asset['symbol'];
  price: Asset['price'];
  quantity: Asset['quantity'];
  type: Asset['type'];
}

export interface IAssetService {
  add(asset: Partial<AssetDTO>): Promise<AssetEntity>;

  findOne(assetId: Asset['id']): Promise<AssetEntity>;
  find(_filters: any): Promise<{
    assets: Asset[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }>;

  // update(assetId: Asset['id'], updates: Partial<AssetDTO>): Promise<Asset>;
  // delete(assetId: Asset['id']): Promise<Asset>;
}

export default class AssetService implements IAssetService {
  private readonly _assetModel: ModelStatic<Model>;

  constructor(assetModel: ModelStatic<Model>) {
    this._assetModel = assetModel;
  }

  private async _findAssetModel(id: string, nonDeleted = false): Promise<Model> {
    const whereOptions: WhereOptions = { id };

    if (nonDeleted) whereOptions.deletedAt = null;

    return this._assetModel
      .findOne({
        where: whereOptions,
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("Asset doesn't exist!");
        }
        return res;
      });
  }

  async add(asset: Partial<AssetDTO>): Promise<AssetEntity> {
    const self = this;

    function tryInsert(_asset: Asset) {
      const categoryEnitity = AssetEntity.create({
        ..._asset,
        id: uuidv4(),
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      return self._assetModel.create(categoryEnitity.toPersistant(), {
        isNewRecord: true,
      });
    }

    function errorCheck(err: Error) {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return true;
      }
      return false;
    }

    const createFunc = retryableDecorator(tryInsert, errorCheck, 10);

    return createFunc(asset)
      .then((res: any) => {
        return AssetEntity.create({
          ...res.dataValues,
        } as Asset);
      })
      .catch((err) => {
        logger.error((err as Error).message, err);
        if (err instanceof ApiError || err instanceof CoreError) {
          throw err;
        }
        throw new Error('Unable to create new Category.');
      });
  }

  async findOne(assetId: string): Promise<AssetEntity> {
    const self = this;

    try {
      const category = await self._findAssetModel(assetId, true);

      return AssetEntity.create(category.toJSON() as Asset);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Category.');
    }
  }

  async find(_filters: any): Promise<{
    assets: Asset[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }> {
    const self = this;

    try {
      const where: WhereOptions = {
        deletedAt: null,
      };

      const filters = _filters || {};
      const limit = filters.pageSize || 10;
      const currentPage = filters.page || 1;
      const offset = (currentPage - 1) * limit;
      const orderBy: Order = [
        ['updatedAt', 'DESC'],
        ['id', 'DESC'],
      ];

      const assets = await self._assetModel.findAndCountAll({
        where,
        order: orderBy,
        limit,
        offset,
      });

      const assetsEntities = AssetEntity.fromJsonArray(assets.rows);

      return {
        assets: assetsEntities,
        totalPage: Math.ceil(assets.count / limit),
        currentPage,
        limit,
      };
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Category.');
    }
  }
}
