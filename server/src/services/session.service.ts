import * as crypto from 'crypto';
import { Model, ModelStatic } from 'sequelize/types';
import { SERVER_CONTEXT } from '../config';
import { Session, SessionData } from '../core/entities/session';
import { Utils as DBUtils } from '../db';
import logger from '../logger';

export interface ISessionService {
  persistInSession(session: SessionData, expiresAt: Date): Promise<Session | null>;
  removeFromSession(sessionId: string): Promise<SessionData | null>;
  getFromSession(sessionId: string): Promise<Session | null>;
  resetExpiry(sessionId: string, expireTime: Date): Promise<Session | null>;
  updateSessionData(sessionId: string, data: SessionData): Promise<Session | null>;
}

export class SessionService implements ISessionService {
  private readonly _store: ModelStatic<Model>;
  private readonly _int: number;

  constructor(store: ModelStatic<Model>) {
    this._store = store;
    this._int = 1000 * 60 * 60;
    this.clearExpiredSessionsInterval();
  }

  clearExpiredSessions(): Promise<void> {
    return this._store
      .destroy({ where: { expiresAt: { [DBUtils.ops.lt]: new Date() } } })
      .then(() => {
        logger.info(SERVER_CONTEXT, 'Expired sessions cleared');
      })
      .catch((e) => {
        logger.error(SERVER_CONTEXT, e);
      });
  }

  clearExpiredSessionsInterval() {
    const interval = setInterval(() => {
      this.clearExpiredSessions();
    }, this._int);
    return {
      clear: () => {
        clearInterval(interval);
      },
    };
  }

  updateSessionData(sessionId: string, data: SessionData): Promise<Session | null> {
    return this.getFromSession(sessionId).then((session) => {
      if (!session) {
        return null;
      }
      session.data = JSON.parse(session.data as string) as SessionData;
      if (session.data.id !== data.id) {
        throw new Error('Session id cannot be changed');
      }
      return this._store
        .update({ data: JSON.stringify(data) }, { where: { id: sessionId } })
        .then((result) => {
          if (result[0] === 0) {
            return null;
          }
          session.data = data;
          return session;
        })
        .catch((e) => {
          logger.error((e as Error).message, e);
          return null;
        });
    });
  }

  removeFromSession(sessionId: string): Promise<SessionData | null> {
    return this.getFromSession(sessionId).then((session) => {
      if (session) {
        return this._store
          .destroy({ where: { id: sessionId } })
          .then(() => JSON.parse(session.data as string) as SessionData)
          .catch((e) => {
            logger.error((e as Error).message, e);
            return null;
          });
      }
      return null;
    });
  }

  getFromSession(sessionId: string): Promise<Session | null> {
    return this._store
      .findByPk(sessionId)
      .then((session) => {
        if (!session) {
          return null;
        }
        const sessionVal = session.toJSON() as Session;
        return {
          data: sessionVal.data,
          expiresAt: sessionVal.expiresAt,
          id: sessionVal.id,
          createdAt: sessionVal.createdAt,
        };
      })
      .catch((e) => {
        logger.error((e as Error).message, e);
        return null;
      });
  }

  resetExpiry(sessionId: string, expireTime: Date): Promise<Session | null> {
    if (expireTime.getTime() < Date.now() + 1000) {
      throw new Error('Session expires before it was created');
    }
    return this._store
      .update({ expiresAt: expireTime }, { where: { id: sessionId } })
      .then((result) => {
        if (result[0] === 0) {
          return null;
        }
        return this.getFromSession(sessionId);
      })
      .catch((e) => {
        logger.error((e as Error).message, e);
        return null;
      });
  }

  async persistInSession(session: SessionData, expiresAt: Date): Promise<Session | null> {
    const sessId = this._getSessionId(session);
    const createdAt = new Date();
    if (expiresAt.getTime() < createdAt.getTime()) {
      throw new Error('Session expires before it was created.');
    }
    try {
      const createdSession = (
        (await this._store.create({
          id: sessId,
          data: JSON.stringify(session),
          expiresAt: expiresAt,
          createdAt: createdAt,
        })) as any
      ).dataValues as Session;
      return {
        data: createdSession.data,
        expiresAt: createdSession.expiresAt,
        id: createdSession.id,
        createdAt: createdSession.createdAt,
      };
    } catch (e: any) {
      logger.error((e as Error).message, e);
      return null;
    }
  }

  private _getSessionId(session: SessionData): string {
    return crypto
      .createHash('sha256')
      .update(`${session.id}$${crypto.randomInt(Math.floor(Math.pow(2, 32)))}`)
      .digest('base64');
  }
}
