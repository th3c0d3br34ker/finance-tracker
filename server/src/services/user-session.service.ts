import { ISessionService } from './session.service';
import * as crypto from 'crypto';
import { SessionData } from '../core/entities/session';

export const USER_SESSION_NAME = 'u_sess';
export const USER_SESSION_SIGN = 'u_sess_sig';
export const USER_SESSION_MAXAGE = 1000 * 60 * 60 * 24 * 7; // 7 days

export interface UserSession {
  id: string;
  signature: string;
}

export type UserSessionData = SessionData;

export interface IUserSessionService {
  createSession(user: UserSessionData): Promise<UserSession>;
  getUserBySession(sessionId: string, signature: string): Promise<UserSessionData>;
  destroyUserSession(sessionId: string, signature: string): Promise<void>;
  touchUserSession(sessionId: string, signature: string, expires: Date): Promise<UserSession>;
}

export class UserSessionService implements IUserSessionService {
  private readonly _sessionService: ISessionService;
  private readonly sessionKey = crypto.createSecretKey(Buffer.from(crypto.randomBytes(64)));

  constructor(sessionService: ISessionService) {
    this._sessionService = sessionService;
  }

  createSession(user: UserSessionData): Promise<UserSession> {
    return this._sessionService
      .persistInSession(
        {
          id: `user${user.id}`,
          user: {
            role: user.role,
            id: user.id,
          },
        },
        new Date(Date.now() + USER_SESSION_MAXAGE),
      )
      .then((session) => {
        if (!session) {
          throw new Error('Session not created');
        }
        return {
          id: session.id,
          signature: this._getSignedSessionId(session.id),
        };
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  getUserBySession(sessionId: string, signature: string): Promise<UserSessionData> {
    if (this._verifySignedSessionId(sessionId, signature)) {
      return this._sessionService
        .getFromSession(sessionId)
        .then((session) => {
          if (!session) {
            throw new Error('Session not found');
          }
          return JSON.parse(session.data as string).user as UserSessionData;
        })
        .catch((err) => {
          throw new Error(err);
        });
    } else {
      throw new Error('Invalid signature');
    }
  }

  destroyUserSession(sessionId: string, signature: string): Promise<void> {
    if (this._verifySignedSessionId(sessionId, signature)) {
      return this._sessionService
        .removeFromSession(sessionId)
        .then(() => {
          return;
        })
        .catch((err) => {
          throw new Error(err);
        });
    } else {
      throw new Error('Invalid signature');
    }
  }

  touchUserSession(sessionId: string, signature: string, expires: Date): Promise<UserSession> {
    if (this._verifySignedSessionId(sessionId, signature)) {
      return this._sessionService
        .resetExpiry(sessionId, expires)
        .then((session) => {
          if (!session) {
            throw new Error('Session not found');
          }
          return {
            id: session.id,
            signature: this._getSignedSessionId(session.id),
          };
        })
        .catch((err) => {
          throw new Error(err);
        });
    } else {
      throw new Error('Invalid signature');
    }
  }

  private _getSignedSessionId(sessionId: string): string {
    return crypto.createHmac('sha256', this.sessionKey).update(sessionId).toString();
  }

  private _verifySignedSessionId(sessionId: string, signedSessionId: string): boolean {
    return Buffer.compare(Buffer.from(this._getSignedSessionId(sessionId)), Buffer.from(signedSessionId)) === 0;
  }
}
