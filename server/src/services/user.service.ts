import { Model, ModelStatic, Order, WhereOptions } from 'sequelize/types';
import UserEntity, { User } from '../core/entities/user';
import { Role } from '../core/roles';
import { v4 as uuidv4 } from 'uuid';
import { retryableDecorator } from '../utils';
import bcrypt from 'bcrypt';
import { Utils as DBUtils } from '../db/index';
import { NotFoundError } from '../errors/api.error';
import logger from '../logger';
import CoreError, { InvalidInputError, InvalidRequestError } from '../core/errors';
import _ from 'lodash';
import validator from 'validator';
import * as config from '../config';

export interface UserDTO {
  name: User['name'];
  email: User['email'];
  password: User['password'];
}

export interface IUserService {
  getAll(filters: any): Promise<{
    data: UserEntity[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }>;
  getById(id: User['id'], nonDeleted: boolean): Promise<UserEntity>;
  getByEmail(email: User['email']): Promise<UserEntity>;
  create(user: UserDTO): Promise<UserEntity>;
  update(id: User['id'], user: UserDTO): Promise<UserEntity>;
  delete(id: User['id']): Promise<UserEntity>;
  resetPassword(id: User['id'], password: User['password']): Promise<UserEntity>;
  resetEmail(id: User['id'], password: User['password']): Promise<UserEntity>;
  getAllowedOrderByFields(): string[];
  login(email: User['email'], password: User['password']): Promise<UserEntity>;
}

export default class UserService implements IUserService {
  private readonly _userModel: ModelStatic<Model>;
  private _superAdmin: UserEntity | null;

  constructor(_model: ModelStatic<Model>) {
    this._userModel = _model;
    this._superAdmin = null;
  }

  login(email: string, password: string): Promise<UserEntity> {
    console.log(email, password);
    if (this._superAdminData().email === email) {
      return this.getSuperAdminData().then(() => {
        if (this._superAdmin !== null && this._superAdmin.isValidPassword(password)) {
          return this._superAdmin;
        }
        throw new InvalidInputError('Invalid password or email');
      });
    }
    return this._findUserModelByEmail(email)
      .then((res) => {
        const user = UserEntity.create(res.toJSON() as User);
        if (!user.isValidPassword(password)) {
          throw new InvalidInputError('Invalid password or email');
        }
        return user;
      })
      .catch(() => {
        throw new InvalidInputError('Invalid password or email');
      });
  }

  resetPassword(id: User['id'], password: User['password']): Promise<UserEntity> {
    return this._findUserModel(id)
      .then((res) => {
        const user = UserEntity.create(res.toJSON() as User);
        user.resetPassword(password);
        return res
          .update({
            password: user.password,
            updatedAt: user.updatedAt,
          })
          .then(() => {
            if (this._superAdmin !== null && this._superAdmin.id === id) {
              this._superAdmin = user;
            }
            return user;
          })
          .catch(() => {
            throw new Error('If email is valid we would have sent a reset request');
          });
      })
      .catch(() => {
        throw new Error('If email is valid we would have sent a reset request');
      });
  }

  resetEmail(id: string, email: string): Promise<UserEntity> {
    return this._findUserModel(id, true).then((res) => {
      const user = UserEntity.create(res.toJSON() as User);
      if (user.email === email) {
        return user;
      }
      if (user.email === this._superAdminData().email) {
        throw new InvalidInputError("Cannot change this user's email");
      }
      user.resetEmail(email);
      return this._checkExistingEmail(email).then((yes) => {
        if (yes) {
          throw new InvalidRequestError('Email already exists');
        }
        return res
          .update(user.toJSON())
          .then(() => {
            if (this._superAdmin !== null && this._superAdmin.id === id) {
              this._superAdmin = user;
            }
            return user;
          })
          .catch(() => {
            throw new Error('Unable to update the email');
          });
      });
    });
  }

  async getAll(filters: any): Promise<{
    data: UserEntity[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }> {
    let whereClause: WhereOptions = {
      deletedAt: null,
    };
    if (filters.search) {
      filters.search = String.prototype.toLowerCase.call(filters.search);
      whereClause = {
        ...whereClause,
        [DBUtils.ops.or]: this.getAllowedOrderByFields().map((field) =>
          DBUtils.getRegexInsensitiveQuery(field, filters.search),
        ),
      };
    }
    const orderBy: Order = [
      ['updatedAt', 'DESC'],
      ['id', 'DESC'],
    ];
    if (
      filters.orderBy &&
      filters.orderBy !== 'updatedAt' &&
      this.getAllowedOrderByFields().includes(filters.orderBy)
    ) {
      orderBy.unshift([filters.orderBy, 'ASC']);
    }
    const limit = filters.pageSize || 20;
    const skip = filters.page && filters.page >= 1 ? (filters.page - 1) * limit : 0;
    return this._userModel
      .findAndCountAll({
        where: whereClause,
        order: orderBy,
        offset: skip,
        limit: limit,
      })
      .then((res) => {
        return {
          data: res.rows.map((user: Model) => UserEntity.create(user.toJSON() as User)),
          totalPage: Math.ceil(res.count / limit),
          limit,
          currentPage: skip / limit + 1,
        };
      })
      .catch((err) => {
        logger.error(err);
        if (err instanceof CoreError) {
          throw err;
        }
        throw new Error('Unable to fetch users');
      });
  }

  getById(id: string, nonDeleted = false): Promise<UserEntity> {
    return this._findUserModel(id, nonDeleted).then((res) => {
      return UserEntity.create(res.toJSON() as User);
    });
  }

  getByEmail(email: User['email']): Promise<UserEntity> {
    return this._findUserModelByEmail(email).then((res) => {
      return UserEntity.create(res.toJSON() as User);
    });
  }

  create(user: UserDTO): Promise<UserEntity> {
    return this._checkExistingEmail(user.email).then((yes) => {
      if (yes) {
        throw new InvalidRequestError('Email already exists');
      }

      const self = this;
      function tryInsert(user: UserDTO) {
        const userEntity = UserEntity.create({
          ...user,
          password: bcrypt.hashSync(user.password, 10),
          id: uuidv4(),
          createdAt: new Date(),
          updatedAt: new Date(),
        });
        return self._userModel
          .create(userEntity.toJSON(true), {
            isNewRecord: true,
          })
          .then(() => userEntity);
      }
      function errorCheck(err: Error) {
        if (err.name === 'SequelizeUniqueConstraintError') {
          return true;
        }
        return false;
      }
      const createFunc = retryableDecorator(tryInsert, errorCheck, 10);
      return createFunc(user)
        .then((user) => {
          return user as UserEntity;
        })
        .catch((err) => {
          logger.error(config.SERVER_CONTEXT, err);
          if (err instanceof CoreError) {
            throw err;
          }
          throw new Error(`Unable to create new User ${err.message}`);
        });
    });
  }

  update(id: string, updates: Partial<UserDTO>): Promise<UserEntity> {
    return this._findUserModel(id, true).then((res) => {
      const user = UserEntity.create(res.toJSON() as User);
      user.update(updates);
      return res
        .update(user.toJSON())
        .then(() => {
          if (this._superAdmin !== null && this._superAdmin.id === id) {
            this._superAdmin = user;
          }
          return user;
        })
        .catch((_err) => {
          if (_err instanceof CoreError) {
            throw _err;
          }
          throw new Error('Unable to update the user');
        });
    });
  }

  delete(id: string): Promise<UserEntity> {
    return this._findUserModel(id, true).then((res) => {
      if (this._superAdmin !== null && this._superAdmin.id === id) {
        throw new InvalidInputError('Cannot delete this user');
      }
      const deletedAt = new Date(Date.now());
      return res
        .update({
          deletedAt,
        })
        .then(() => {
          return UserEntity.create(res.toJSON() as User);
        })
        .catch((err) => {
          if (err instanceof CoreError) {
            throw err;
          }
          throw new Error(`Unable to delete: ${err.message || err.toString()}`);
        });
    });
  }

  getAllowedOrderByFields(): string[] {
    return ['name', 'email'];
  }

  async getSuperAdminData(): Promise<UserEntity> {
    try {
      if (_.isNull(this._superAdmin)) {
        try {
          const exist = await this._userModel.findOne({
            where: {
              role: Role.SUPER_ADMIN,
            },
            rejectOnEmpty: true,
          });
          if ((exist.toJSON() as User).email !== this._superAdminData().email) {
            await exist.destroy();
            throw new CoreError('Super admin data is not valid');
          }
          this._superAdmin = UserEntity.create(exist.toJSON() as User);
        } catch (err) {
          this._superAdmin = await this.create(this._superAdminData());
        }
      }
      return this._superAdmin;
    } catch (err) {
      logger.error('super-admin-creation', err);
      throw new NotFoundError('User not found');
    }
  }

  // private methods
  private _superAdminData(): UserDTO {
    return {
      email: config.SUPER_ADMIN_EMAIL,
      name: 'Super Admin',
      password: config.SUPER_ADMIN_PASSWORD,
    };
  }

  private async _checkExistingEmail(email: User['email']): Promise<boolean> {
    try {
      await this._userModel.findOne({
        where: {
          email,
          deletedAt: null,
        },
        rejectOnEmpty: true,
      });
      return true;
    } catch (_err) {
      return false;
    }
  }

  private _findUserModel(id: User['id'], nonDeleted = false): Promise<Model> {
    if (!validator.isUUID(id)) {
      throw new NotFoundError('User not found');
    }
    const whereOptions: WhereOptions = nonDeleted ? { id, deletedAt: null } : { id };
    return this._userModel
      .findOne({
        where: whereOptions,
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("User doesn't exist");
        }
        return res;
      });
  }

  private _findUserModelByEmail(email: User['email']): Promise<Model> {
    if (!validator.isEmail(email)) {
      throw new NotFoundError('User not found');
    }
    return this._userModel
      .findOne({
        where: {
          email,
          deletedAt: null,
        },
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("User doesn't exist");
        }
        return res;
      });
  }
}
