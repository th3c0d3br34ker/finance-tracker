import { v4 as uuidv4 } from 'uuid';
import { Model, ModelStatic, Order, WhereOptions } from 'sequelize/types';
import validator from 'validator';

import { retryableDecorator } from '../utils';
import logger from '../logger';
import CoreError from '../core/errors';
import AccountEntity, { Account } from '../core/entities/account';
import CreditAccountEntity, { CreditAccount } from '../core/entities/credit-account';
import ApiError, { NotFoundError } from '../errors/api.error';

import AccountTypes from '../core/accounts-types';

export interface AccountDTO {
  name: Account['name'];
  type: Account['type'];
  user: Account['user'];
  accountNumber: Account['accountNumber'];
  openingBalance: Account['openingBalance'];
  balance: Account['balance'];
}

export interface CreditAccountDTO {
  name: CreditAccount['name'];
  type: CreditAccount['type'];
  user: CreditAccount['user'];
  accountNumber: CreditAccount['accountNumber'];
  creditLimit: CreditAccount['creditLimit'];
  balance: Account['balance'];
}

export interface IAccountService {
  add(account: Partial<AccountDTO | CreditAccountDTO>): Promise<AccountEntity | CreditAccountEntity>;

  findOne(accountId: Account['id']): Promise<AccountEntity | CreditAccountEntity>;
  find(_filters: any): Promise<{
    accounts: (AccountEntity | CreditAccountEntity)[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }>;

  update(
    accountId: Account['id'],
    updates: Partial<AccountDTO | CreditAccountDTO>,
  ): Promise<AccountEntity | CreditAccountEntity>;
  delete(accountId: Account['id']): Promise<AccountEntity | CreditAccountEntity>;
}

export default class AccountService implements IAccountService {
  private readonly _accountModel: ModelStatic<Model>;
  private readonly _creditAccountModel: ModelStatic<Model>;

  constructor(accountModel: ModelStatic<Model>, creditAccountModel: ModelStatic<Model>) {
    this._accountModel = accountModel;
    this._creditAccountModel = creditAccountModel;
  }

  private async _findAccountModel(id: string, nonDeleted = false): Promise<{ isCredit: boolean; model: Model }> {
    const account = await this._findDebitAccountModel(id, nonDeleted);

    if (account) {
      return { isCredit: false, model: account };
    }

    const creditAccount = await this._findCreditAccountModel(id, nonDeleted);

    if (creditAccount) {
      return { isCredit: true, model: creditAccount };
    }

    throw new NotFoundError("Account doesn't exist!");
  }

  private _findDebitAccountModel(id: Account['id'], nonDeleted = false): Promise<Model> {
    if (!validator.isUUID(id)) {
      throw new NotFoundError('Account not found!');
    }
    const whereOptions: WhereOptions = nonDeleted ? { id, deletedAt: null } : { id };
    return this._accountModel
      .findOne({
        where: whereOptions,
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("Account doesn't exist!");
        }
        return res;
      });
  }

  private _findCreditAccountModel(id: CreditAccount['id'], nonDeleted = false): Promise<Model> {
    if (!validator.isUUID(id)) {
      throw new NotFoundError('Account not found!');
    }
    const whereOptions: WhereOptions = nonDeleted ? { id, deletedAt: null } : { id };
    return this._creditAccountModel
      .findOne({
        where: whereOptions,
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("Account doesn't exist!");
        }
        return res;
      });
  }

  async add(account: Partial<AccountDTO | CreditAccountDTO>): Promise<AccountEntity | CreditAccountEntity> {
    const self = this;

    async function tryInsert(_account: AccountDTO | CreditAccountDTO) {
      let data;
      let isCredit = false;

      if (AccountTypes.includes(_account.type)) {
        const accountEnitity = AccountEntity.create({
          ..._account,
          id: uuidv4(),
          createdAt: new Date(),
          updatedAt: new Date(),
        });

        data = await self._accountModel.create(accountEnitity.toPersistant(), {
          isNewRecord: true,
        });
      } else {
        isCredit = true;
        const creditAccountEnitity = CreditAccountEntity.create({
          ..._account,
          id: uuidv4(),
          createdAt: new Date(),
          updatedAt: new Date(),
        });

        data = await self._creditAccountModel.create(creditAccountEnitity.toPersistant(), {
          isNewRecord: true,
        });
      }

      return { data: data.dataValues, isCredit };
    }

    function errorCheck(err: Error) {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return true;
      }
      if (err.name === 'SequelizeDatabaseError') {
        return true;
      }
      return false;
    }

    const createFunc = retryableDecorator(tryInsert, errorCheck, 10);

    try {
      const accountRecord: any = await createFunc(account);
      if (accountRecord.isCredit) {
        return CreditAccountEntity.create({
          ...accountRecord.data,
        } as CreditAccount);
      } else {
        return AccountEntity.create({
          ...accountRecord.data,
        } as Account);
      }
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof CoreError) {
        throw err;
      }
      throw new Error('Unable to create new Account.');
    }
  }

  async findOne(accountId: string): Promise<AccountEntity | CreditAccountEntity> {
    const self = this;

    try {
      const account = await self._findAccountModel(accountId, true);

      if (account?.isCredit) {
        return await CreditAccountEntity.create(account.model.toJSON() as CreditAccount);
      } else {
        return await AccountEntity.create(account.model.toJSON() as Account);
      }
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Account.');
    }
  }

  async find(_filters: any): Promise<{
    accounts: (AccountEntity | CreditAccountEntity)[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }> {
    try {
      const where: WhereOptions = {
        deletedAt: null,
      };

      const filters = _filters || {};
      const limit = filters.pageSize || 10;
      const currentPage = filters.page || 1;
      const offset = (currentPage - 1) * limit;
      const orderBy: Order = [
        ['updatedAt', 'DESC'],
        ['id', 'DESC'],
      ];

      const accounts = await this._accountModel.findAndCountAll({
        where,
        order: orderBy,
        limit,
        offset,
      });

      const accountsEntities = accounts.rows.map((account) => {
        return AccountEntity.create(account.toJSON() as Account);
      });

      const creditAccounts = await this._creditAccountModel.findAndCountAll({
        where,
        order: orderBy,
        limit,
        offset,
      });

      const creditAccountsEntities = creditAccounts.rows.map((creditAccount) => {
        return CreditAccountEntity.create(creditAccount.toJSON() as CreditAccount);
      });

      return {
        accounts: [...accountsEntities, ...creditAccountsEntities],
        totalPage: Math.ceil(accounts.count / limit),
        currentPage,
        limit,
      };
    } catch (err) {
      logger.error((err as Error).message, err);
      throw new Error('Unable to fetch Accounts.');
    }
  }

  async update(accountId: string, updates: Partial<AccountDTO>): Promise<AccountEntity | CreditAccountEntity> {
    const self = this;
    try {
      const account = await self._findAccountModel(accountId, true);
      account.model = await account.model.update(updates);

      if (account?.isCredit) {
        return await CreditAccountEntity.create(account?.model.toJSON() as CreditAccount);
      } else {
        return await AccountEntity.create(account.model.toJSON() as Account);
      }
    } catch (err) {
      logger.error((err as Error).message, err);
      throw new Error('Unable to update Account.');
    }
  }

  async delete(accountId: string): Promise<AccountEntity | CreditAccountEntity> {
    const self = this;
    try {
      const account = await self._findAccountModel(accountId, true);

      account.model = await account.model.update({ deletedAt: new Date(Date.now()) });

      if (account?.isCredit) {
        return await CreditAccountEntity.create(account.model.toJSON() as CreditAccount);
      } else {
        return await AccountEntity.create(account.model.toJSON() as Account);
      }
    } catch (err) {
      logger.error((err as Error).message, err);
      throw new Error('Unable to delete Account.');
    }
  }
}
