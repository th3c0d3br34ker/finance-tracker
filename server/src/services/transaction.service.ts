import { v4 as uuidv4 } from 'uuid';
import { Model, ModelStatic, Order, WhereOptions } from 'sequelize/types';
import validator from 'validator';

import TransactionEntity, { Transaction } from '../core/entities/transaction';
import CategoryService from './category.service';
import AccountService from './account.service';
import logger from '../logger';
import { retryableDecorator } from '../utils';
import ApiError, { NotFoundError } from '../errors/api.error';
import { accountModel } from '../loaders/account.loaders';
import { categoryModel } from '../loaders/category.loaders';
import CoreError from '../core/errors';

export interface TransactionDTO {
  type: Transaction['type'];
  amount: Transaction['amount'];
  account: Transaction['account'];
  category: Transaction['category'];
  description: Transaction['description'];
  recurring: Transaction['recurring'];
}

export interface ITransactionService {
  add(transaction: TransactionDTO): Promise<TransactionEntity>;

  findOne(transactionId: Transaction['id']): Promise<TransactionEntity>;
  find(_filters: any): Promise<{
    transactions: TransactionEntity[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }>;

  update(transactionId: Transaction['id'], updates: Partial<TransactionDTO>): Promise<TransactionEntity>;
  delete(transactionId: Transaction['id']): Promise<TransactionEntity>;
}

export default class TransactionService implements ITransactionService {
  private readonly _transactionModel: ModelStatic<Model>;
  protected readonly _categoryService: CategoryService;
  protected readonly _accountService: AccountService;

  constructor(model: ModelStatic<Model>, categoryService: CategoryService, accountService: AccountService) {
    this._transactionModel = model;
    this._categoryService = categoryService;
    this._accountService = accountService;
  }

  private _findTransactionModel(id: Transaction['id'], nonDeleted = false): Promise<Model> {
    if (!validator.isUUID(id)) {
      throw new NotFoundError('Invalid transactionId found!');
    }
    const whereOptions: WhereOptions = nonDeleted ? { id, deletedAt: null } : { id };
    return this._transactionModel
      .findOne({
        where: whereOptions,
        include: [accountModel, categoryModel],
      })
      .then((res) => {
        if (res === null) {
          throw new NotFoundError("Transaction doesn't exist!");
        }
        return res;
      });
  }

  async add(transaction: TransactionDTO): Promise<TransactionEntity> {
    const self = this;

    function tryInsert(_transaction: Transaction) {
      const transactionEnitity = TransactionEntity.create({
        ..._transaction,
        id: uuidv4(),
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      return self._transactionModel.create(transactionEnitity.toPersistant(), {
        isNewRecord: true,
      });
    }

    function errorCheck(err: Error) {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return true;
      }
      return false;
    }

    const createFunc = retryableDecorator(tryInsert, errorCheck, 10);

    try {
      const transactionRecord: any = await createFunc(transaction);

      return TransactionEntity.create({
        ...transactionRecord.dataValues,
      } as Transaction);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError || err instanceof CoreError) {
        throw err;
      }
      throw new Error('Unable to create new Transaction.');
    }
  }

  async findOne(transactionId: string): Promise<TransactionEntity> {
    const self = this;

    try {
      const transaction: any = await self._findTransactionModel(transactionId, true);

      const category = await this._categoryService.findOne(transaction.category);
      const account = await this._accountService.findOne(transaction.account);

      return TransactionEntity.create({
        ...transaction.toJSON(),
        category: category ? category.toJSON() : null,
        account: account ? account.toJSON() : null,
      } as Transaction);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error(`Unable to fetch Transaction: ${transactionId}.`);
    }
  }

  async find(_filters: any): Promise<{
    transactions: TransactionEntity[];
    totalPage: number;
    currentPage: number;
    limit: number;
  }> {
    const self = this;

    try {
      const where: WhereOptions = {
        deletedAt: null,
      };

      const filters = _filters || {};
      const limit = filters.pageSize || 10;
      const currentPage = filters.page || 1;
      const offset = (currentPage - 1) * limit;
      const orderBy: Order = [
        ['updatedAt', 'DESC'],
        ['id', 'DESC'],
      ];

      const transactions = await self._transactionModel.findAndCountAll({
        where,
        order: orderBy,
        limit,
        offset,
        include: [accountModel, categoryModel],
      });

      const transactionsEntities = transactions.rows.map((transaction: any) => {
        return TransactionEntity.create({
          ...transaction.toJSON(),
        } as Transaction);
      });

      return {
        transactions: transactionsEntities,
        totalPage: Math.ceil(transactions.count / limit),
        currentPage,
        limit,
      };
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to fetch Transactions.');
    }
  }

  async update(transactionId: string, updates: Partial<TransactionDTO>): Promise<TransactionEntity> {
    const self = this;
    try {
      let transaction = await self._findTransactionModel(transactionId);

      transaction = await transaction.update(updates);

      return TransactionEntity.create(transaction.toJSON() as Transaction);
    } catch (err) {
      logger.error((err as Error).message, err);
      throw new Error('Unable to update Transaction.');
    }
  }

  async delete(transactionId: string): Promise<TransactionEntity> {
    const self = this;
    try {
      let transaction = await self._findTransactionModel(transactionId, true);

      const deletedAt = new Date(Date.now());
      transaction = await transaction.update({ deletedAt });

      return TransactionEntity.create(transaction.toJSON() as Transaction);
    } catch (err) {
      logger.error((err as Error).message, err);
      if (err instanceof ApiError) {
        throw err;
      }
      throw new Error('Unable to delete Transaction.');
    }
  }
}
