import _ from 'lodash';
import { InvalidInputError } from '../errors';
import AssetEntity from './asset';

export interface Portfolio {
  id: string;
  name: string;
  assets?: AssetEntity[];
  user: string;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class PortfolioEntity implements Portfolio {
  private _id: string;
  private _name: string;
  private _assets?: AssetEntity[];
  private _user: string;

  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: Portfolio) {
    this._id = data.id;
    this._name = data.name.trim();
    this._assets = data.assets && AssetEntity.fromJsonArray(data.assets);
    this._user = data.user;

    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  public updateDate(): void {
    this._updatedAt = new Date();
  }

  get id(): string {
    return this._id;
  }
  get name(): string {
    return this._name;
  }
  get assets(): AssetEntity[] | undefined {
    return this._assets;
  }
  get user(): string {
    return this._user;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get updatedAt(): Date | string {
    return this._updatedAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public static create(data: Portfolio): PortfolioEntity {
    if (_.isNil(data.name) || _.isEmpty(data.name)) {
      throw new InvalidInputError('Portfolio name is required.');
    }

    if (_.isNil(data.user) || _.isEmpty(data.user)) {
      throw new InvalidInputError('Portfolio user account is required.');
    }

    return new PortfolioEntity({
      id: data.id,
      name: data.name,
      assets: data.assets,
      user: data.user,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      deletedAt: data.deletedAt,
    });
  }

  public toPersistant(): Partial<Portfolio> {
    return {
      id: this._id,
      name: this._name,
      // assets: this._assets?.map(() => AssetEntity.toPersistant()),
      user: this._user,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public toJSON(): Partial<Portfolio> {
    return {
      id: this._id,
      name: this._name,
      assets: this._assets?.map((asset) => asset.toJSON() as AssetEntity),
      user: this._user,
    };
  }
}

export default PortfolioEntity;
