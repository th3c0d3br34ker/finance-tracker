import _ from 'lodash';
import { InvalidInputError } from '../errors/';

export interface CreditAccount {
  id: string;
  name: string;
  type: string;
  user: string;
  accountNumber: string;
  creditLimit: number;
  balance: number;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class CreditAccountEntity implements CreditAccount {
  private _id: string;
  private _name: string;
  private _type: string;
  private _user: string;
  private _accountNumber: string;
  private _creditLimit: number;
  private _balance: number;
  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: CreditAccount) {
    this._id = data.id;
    this._name = data.name;
    this._type = data.type;
    this._user = data.user;
    this._accountNumber = data.accountNumber;
    this._creditLimit = data.creditLimit;
    this._balance = data.balance;
    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  get updatedAt(): Date | string {
    return this._updatedAt;
  }
  get id(): string {
    return this._id;
  }
  get name(): string {
    return this._name;
  }
  get type(): string {
    return this._type;
  }
  get user(): string {
    return this._user;
  }
  get accountNumber(): string {
    return this._accountNumber;
  }
  get creditLimit(): number {
    return this._creditLimit;
  }
  get balance(): number {
    return this._balance;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public updateDate() {
    this._updatedAt = new Date();
  }

  /**
   * create
   */
  public static create(json: any): CreditAccountEntity {
    if (_.isNil(json.updatedAt)) {
      json.updatedAt = new Date();
    }
    if (_.isNil(json.name) || typeof json.name !== 'string' || json.name.trim().length === 0) {
      throw new InvalidInputError('Name must be valid.');
    }
    if (_.isNil(json.user) || typeof json.name !== 'string' || json.name.trim().length === 0) {
      throw new InvalidInputError('Account Type is missing.');
    }
    if (json.type !== 'credit-card') {
      throw new InvalidInputError('Account Type is invalid.');
    }
    if (_.isNil(json.creditLimit)) {
      throw new InvalidInputError('Credit Limit is required.');
    }

    return new CreditAccountEntity({
      id: json.id,
      name: json.name,
      type: json.type,
      user: json.user,
      accountNumber: json.accountNumber,
      creditLimit: json.creditLimit,
      balance: json._balance,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public toPersistant(): Partial<CreditAccount> {
    return {
      id: this._id,
      name: this._name,
      type: this._type,
      user: this._user,
      accountNumber: this._accountNumber,
      creditLimit: this._creditLimit,
      balance: this._balance,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public toJSON(): Partial<CreditAccount> {
    return {
      id: this._id,
      name: this._name,
      type: this._type,
      user: this._user,
      accountNumber: this._accountNumber,
      creditLimit: this._creditLimit,
    };
  }
}

export default CreditAccountEntity;
