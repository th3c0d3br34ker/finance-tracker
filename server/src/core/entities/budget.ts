import _ from 'lodash';
import { InvalidInputError } from '../errors';

export interface Budget {
  id: string;
  name: string;
  user: string;
  account: string;
  amount: number;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class BudgetEntity implements Budget {
  private _id: string;
  private _name: string;
  private _user: string;
  private _account: string;
  private _amount: number;
  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: Budget) {
    this._id = data.id;
    this._name = data.name;
    this._user = data.user;
    this._account = data.account;
    this._amount = data.amount;
    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  get updatedAt(): Date | string {
    return this._updatedAt;
  }

  get id(): string {
    return this._id;
  }
  get name(): string {
    return this._name;
  }
  get user(): string {
    return this._user;
  }
  get account(): string {
    return this._account;
  }
  get amount(): number {
    return this._amount;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public updateDate() {
    this._updatedAt = new Date();
  }

  static create(json: Budget): BudgetEntity {
    if (_.isNil(json.updatedAt)) {
      json.updatedAt = new Date();
    }

    if (_.isNil(json.name) || typeof json.name !== 'string' || json.name.trim().length === 0) {
      throw new InvalidInputError('Name must be valid.');
    }
    if (_.isNil(json.user) || typeof json.user !== 'string' || json.user.trim().length === 0) {
      throw new InvalidInputError('User must be valid.');
    }
    if (_.isNil(json.account) || typeof json.account !== 'string' || json.account.trim().length === 0) {
      throw new InvalidInputError('Account must be valid.');
    }
    if (_.isNil(json.amount) || typeof json.amount !== 'number') {
      throw new InvalidInputError('Amount must be valid.');
    }

    return new BudgetEntity({
      id: json.id,
      name: json.name,
      user: json.user,
      amount: json.amount,
      account: json.account,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public toPersistant(): Partial<BudgetEntity> {
    return {
      id: this._id,
      name: this._name,
      user: this._user,
      account: this._account,
      amount: this._amount,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public toJSON(): Partial<BudgetEntity> {
    return {
      id: this._id,
      name: this._name,
      user: this._user,
      account: this._account,
      amount: this._amount,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }
}

export default BudgetEntity;
