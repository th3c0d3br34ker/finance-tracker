import _ from 'lodash';
import { InvalidInputError } from '../errors';
import { isValidTransactionType } from '../transaction-types';
import { validate } from 'uuid';

export interface Category {
  id: string;
  name: string;
  type: string;
  description: string;
  color: string;
  slug: string;
  parent: string | null;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class CategoryEntity implements Category {
  private _id: string;
  private _name: string;
  private _type: string;
  private _description: string;
  private _color: string;
  private _slug: string;
  private _parent: string | null;

  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: Category) {
    this._id = data.id;
    this._name = data.name.trim();
    this._type = data.type.trim();
    this._description = data.description?.trim();
    this._color = data.color?.trim();
    this._slug = data.slug.trim();
    this._parent = data.parent ? data.parent.trim() : null;

    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  public updateDate(): void {
    this._updatedAt = new Date();
  }

  get id(): string {
    return this._id;
  }
  get name(): string {
    return this._name;
  }
  get type(): string {
    return this._type;
  }
  get description(): string {
    return this._description;
  }
  get color(): string {
    return this._color;
  }
  get slug(): string {
    return this._slug;
  }
  get parent(): string {
    return this.parent;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get updatedAt(): Date | string {
    return this._updatedAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public static create(json: Category): CategoryEntity {
    if (_.isNil(json.name) || _.isEmpty(json.name)) {
      throw new InvalidInputError('Category name is required.');
    }
    if (_.isNil(json.type) || _.isEmpty(json.type)) {
      throw new InvalidInputError('Category type is required.');
    }
    if (!isValidTransactionType(json.type)) {
      throw new InvalidInputError(`Category type: ${json.type} invalid.`);
    }
    if (_.isNil(json.slug) || _.isEmpty(json.slug)) {
      throw new InvalidInputError('Category slug is required.');
    }
    if (json.parent && !validate(json.parent)) {
      throw new InvalidInputError('Invalid parent id.');
    }

    return new CategoryEntity({
      id: json.id,
      name: json.name,
      type: json.type,
      description: json.description,
      color: json.color,
      slug: json.slug,
      parent: json.parent,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public toPersistant(): Partial<Category> {
    return {
      id: this._id,
      name: this._name,
      type: this._type,
      description: this._description,
      color: this.color,
      slug: this._slug,
      parent: this._parent,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public static fromJsonArray(json: any[]): Category[] {
    return json.map(CategoryEntity.create);
  }

  public toJSON(): Partial<Category> {
    return {
      id: this._id,
      name: this._name,
      type: this._type,
      description: this._description,
      color: this._color,
      slug: this._slug,
      parent: this._parent,
    };
  }
}

export default CategoryEntity;
