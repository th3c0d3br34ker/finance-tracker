import _ from 'lodash';
import { isAssetType } from '../asset-types';
import { InvalidInputError } from '../errors';

export interface Asset {
  id: string;
  name: string;
  symbol: string;
  price: number;
  quantity: number;
  type: string;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class AssetEntity implements Asset {
  private _id: string;
  private _name: string;
  private _symbol: string;
  private _price: number;
  private _quantity: number;
  private _type: string;

  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: Asset) {
    this._id = data.id;
    this._name = data.name.trim();
    this._symbol = data.symbol.trim();
    this._price = data.price;
    this._quantity = data.quantity;
    this._type = data.type;

    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  public updateDate(): void {
    this._updatedAt = new Date();
  }

  get id(): string {
    return this._id;
  }
  get name(): string {
    return this._name;
  }
  get symbol(): string {
    return this._symbol;
  }
  get price(): number {
    return this._price;
  }
  get quantity(): number {
    return this._quantity;
  }
  get type(): string {
    return this._type;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get updatedAt(): Date | string {
    return this._updatedAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public static create(json: Asset): AssetEntity {
    if (_.isNil(json.name) || _.isEmpty(json.name)) {
      throw new InvalidInputError('Asset name is required.');
    }

    if (_.isNil(json.symbol) || _.isEmpty(json.symbol)) {
      throw new InvalidInputError('Asset symbol is required.');
    }

    if (_.isNil(json.price) || _.isEmpty(json.price)) {
      throw new InvalidInputError('Asset price is required.');
    }

    if (_.isNil(json.quantity) || _.isEmpty(json.quantity)) {
      throw new InvalidInputError('Asset quantity is required.');
    }

    if (_.isNil(json.type) || _.isEmpty(json.type) || !isAssetType(json.type)) {
      throw new InvalidInputError('Asset type is required.');
    }

    return new AssetEntity({
      id: json.id,
      name: json.name,
      symbol: json.symbol,
      price: json.price,
      quantity: json.quantity,
      type: json.type,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public toPersistant(): Partial<Asset> {
    return {
      id: this._id,
      name: this._name,
      symbol: this._symbol,
      price: this._price,
      quantity: this._quantity,
      type: this._type,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public static fromJsonArray(json: any[]): AssetEntity[] {
    return json.map(AssetEntity.create);
  }

  public toJSON(): Partial<Asset> {
    return {
      id: this._id,
      name: this._name,
      symbol: this._symbol,
      price: this._price,
      quantity: this._quantity,
      type: this._type,
    };
  }
}

export default AssetEntity;
