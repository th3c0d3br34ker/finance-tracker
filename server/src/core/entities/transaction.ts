import _ from 'lodash';
import { InvalidInputError } from '../errors';
import { isValidTransactionType } from '../transaction-types';

export interface Transaction {
  id: string;
  type: string;
  description: string;
  amount: number;
  account: string;
  category: string;
  recurring: boolean;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

export class TransactionEntity implements Transaction {
  private _id: string;
  private _type: string;
  private _description: string;
  private _account: string;
  private _amount: number;
  private _category: string;
  private _recurring: boolean;
  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: Transaction) {
    this._id = data.id;
    this._type = data.type;
    this._description = data.description.trim();
    this._account = data.account;
    this._amount = Number(data.amount);

    this._category = data.category;
    this._recurring = Boolean(data.recurring);
    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  public updateDate(): void {
    this._updatedAt = new Date();
  }

  get id(): string {
    return this._id;
  }
  get description(): string {
    return this._description;
  }
  get type(): string {
    return this._type;
  }
  get account(): string {
    return this._account;
  }

  get category(): string {
    return this._category;
  }
  get amount(): number {
    return this._amount;
  }
  get recurring(): boolean {
    return this._recurring;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get updatedAt(): Date | string {
    return this._updatedAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public static create(json: Transaction): TransactionEntity {
    if (_.isNil(json.type) || _.isEmpty(json.type)) {
      throw new InvalidInputError('Transaction type is required.');
    }
    if (!isValidTransactionType(json.type)) {
      throw new InvalidInputError(`Transaction type: ${json.type} invalid.`);
    }
    if (_.isNil(json.account) || _.isEmpty(json.account)) {
      throw new InvalidInputError('Account is required.');
    }
    if (_.isNil(json.category)) {
      throw new InvalidInputError('Category is required.');
    }
    if (_.isNil(json.amount) || Number.isNaN(Number(json.amount)) || Number(json.amount) <= 0) {
      throw new InvalidInputError('Amount is required.');
    }

    return new TransactionEntity({
      id: json.id,
      type: json.type,
      description: json.description,
      account: json.account,
      amount: json.amount,
      category: json.category,
      recurring: json.recurring,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public toPersistant(): Partial<Transaction> {
    return {
      id: this._id,
      description: this._description,
      type: this._type,
      amount: this._amount,
      account: this._account,
      category: this._category,
      recurring: this.recurring,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public static fromJsonArray(json: any[]): Transaction[] {
    return json.map(TransactionEntity.create);
  }

  public toJSON(): Partial<Transaction> {
    return {
      id: this._id,
      type: this._type,
      amount: this._amount,
      account: this._account,
      category: this._category,
      recurring: this.recurring,
      description: this._description,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
    };
  }
}

export default TransactionEntity;
