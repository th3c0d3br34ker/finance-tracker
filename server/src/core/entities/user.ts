import bcrypt from 'bcrypt';
import validator from 'validator';
import { InvalidInputError } from '../errors';
import _ from 'lodash';

export interface User {
  id: string;
  name: string;
  email: string;
  password: string;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class UserEntity implements User {
  _id: string;
  _name: string;
  _email: string;
  _password: string;
  _createdAt: Date | string;
  _updatedAt: Date | string;
  _deletedAt: Date | string | null;

  private constructor(user: User) {
    this._id = user.id;
    this._name = user.name.trim();
    this._email = user.email.trim();
    this._password = user.password;
    this._createdAt = user.createdAt;
    this._updatedAt = user.updatedAt;
    this._deletedAt = user.deletedAt || null;

    if (this._deletedAt !== null) {
      Object.freeze(this);
    }
  }

  get id(): string {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
    this.updateDate();
  }

  get email(): string {
    return this._email;
  }

  get password(): string {
    return this._password;
  }

  get createdAt(): Date | string {
    return this._createdAt;
  }

  get updatedAt(): Date | string {
    return this._updatedAt;
  }

  get deletedAt(): Date | null | string {
    return this._deletedAt;
  }

  public resetPassword(password: string): void {
    if (validator.isEmpty(password)) {
      throw new InvalidInputError('Password is empty');
    }
    this._password = bcrypt.hashSync(password, 10);
    this.updateDate();
  }

  public resetEmail(email: string): void {
    if (_.isEmpty(email)) {
      throw new InvalidInputError('Email is empty');
    }
    if (!validator.isEmail(email)) {
      throw new InvalidInputError('Invalid email');
    }
    this._email = email;
    this.updateDate();
  }

  public isValidPassword(password: string): boolean {
    return bcrypt.compareSync(password, this._password);
  }

  public update(updates: Partial<User>) {
    if (!_.isNil(updates.name)) {
      if (validator.isEmpty(updates.name)) {
        throw new InvalidInputError('Name is empty');
      } else {
        this._name = updates.name;
      }
    }
    this.updateDate();
  }

  public updateDate() {
    this._updatedAt = new Date();
  }

  public static create(json: any): UserEntity {
    if (!validator.isEmail(json.email)) {
      throw new InvalidInputError('Invalid email');
    }
    if (validator.isEmpty(json.password)) {
      throw new InvalidInputError('Invalid password');
    }
    if (validator.isEmpty(json.name) || typeof json?.name !== 'string' || json?.name.trim().length <= 0) {
      throw new InvalidInputError("Invalid name, it can't be empty");
    }
    if (_.isNil(json.createdAt)) {
      json.createdAt = new Date();
    }
    if (_.isNil(json.updatedAt)) {
      json.updatedAt = new Date();
    }
    if (!_.isNil(json.deletedAt)) {
      json.deletedAt = new Date(json.deletedAt);
    }

    return new UserEntity({
      id: json.id,
      name: json.name,
      email: json.email,
      password: json.password,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public static fromJsonArray(json: any[]): UserEntity[] {
    return json.map(UserEntity.create);
  }

  public toJSON(showPass = false): Partial<User> {
    const data: any = {
      id: this.id,
      name: this.name,
      email: this.email,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
    if (showPass) {
      data.password = this.password;
    }
    return data;
  }

  public toOtherJSON(): Partial<User> {
    return {
      id: this.id,
      name: this.name,
      email: this.email,
    };
  }
}

export default UserEntity;
