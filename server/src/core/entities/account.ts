import _ from 'lodash';
import { isValidAccountType } from '../accounts-types';
import { InvalidInputError } from '../errors';

export interface Account {
  id: string;
  name: string;
  type: string;
  user: string;
  accountNumber: string;
  openingBalance: number;
  balance: number;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string | null;
}

class AccountEntity implements Account {
  private _id: string;
  private _name: string;
  private _type: string;
  private _user: string;
  private _accountNumber: string;
  private _openingBalance: number;
  private _balance: number;
  private _createdAt: Date | string;
  private _updatedAt: Date | string;
  private _deletedAt: Date | string | null;

  private constructor(data: Account) {
    this._id = data.id;
    this._name = data.name;
    this._type = data.type;
    this._user = data.user;
    this._accountNumber = data.accountNumber;
    this._openingBalance = data.openingBalance;
    this._balance = data.balance;
    this._createdAt = data.createdAt;
    this._updatedAt = data.updatedAt;
    this._deletedAt = data.deletedAt || null;

    if (!_.isNull(this._deletedAt)) {
      Object.freeze(this);
    }
  }

  get updatedAt(): Date | string {
    return this._updatedAt;
  }
  get id(): string {
    return this._id;
  }
  get name(): string {
    return this._name;
  }
  get type(): string {
    return this._type;
  }
  get user(): string {
    return this._user;
  }
  get accountNumber(): string {
    return this._accountNumber;
  }
  get openingBalance(): number {
    return this._openingBalance;
  }
  get balance(): number {
    return this._balance;
  }
  set balance(newBalance: number) {
    this._balance = newBalance;
  }
  get createdAt(): Date | string {
    return this._createdAt;
  }
  get deletedAt(): Date | string | null {
    return this._deletedAt;
  }

  public updateDate() {
    this._updatedAt = new Date();
  }

  /**
   * create
   */
  public static create(json: any): AccountEntity {
    if (_.isNil(json.updatedAt)) {
      json.updatedAt = new Date();
    }

    if (_.isNil(json.user) || typeof json.name !== 'string' || json.name.trim().length === 0) {
      throw new InvalidInputError('Account must have a user.');
    }
    if (_.isNil(json.accountNumber)) {
      throw new InvalidInputError('Account number is required.');
    }
    if (_.isNil(json.name) || typeof json.name !== 'string' || json.name.trim().length === 0) {
      throw new InvalidInputError('Name must be valid.');
    }
    if (_.isNil(json.openingBalance)) {
      throw new InvalidInputError('Opening balance is required.');
    }
    if (json.openingBalance < 0) {
      throw new InvalidInputError('Opening balance cannot be negative.');
    }
    if (_.isNil(json.balance)) {
      json.balance = json.openingBalance;
    }
    if (_.isNil(json.type)) {
      throw new InvalidInputError('Account type is required.');
    }
    if (!isValidAccountType(json.type)) {
      throw new InvalidInputError('Account Type is missing or invalid.');
    }

    return new AccountEntity({
      id: json.id,
      name: json.name,
      type: json.type,
      user: json.user,
      accountNumber: json.accountNumber,
      openingBalance: json.openingBalance,
      balance: json.balance,
      createdAt: json.createdAt,
      updatedAt: json.updatedAt,
      deletedAt: json.deletedAt,
    });
  }

  public toPersistant(): Partial<Account> {
    return {
      id: this._id,
      name: this._name,
      type: this._type,
      user: this._user,
      accountNumber: this._accountNumber,
      openingBalance: this._openingBalance,
      balance: this._balance,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
  }

  public toJSON(): Partial<Account> {
    return {
      id: this._id,
      name: this._name,
      type: this._type,
      user: this._user,
      accountNumber: this._accountNumber,
      openingBalance: this._openingBalance,
      balance: this._balance,
    };
  }
}

export default AccountEntity;
