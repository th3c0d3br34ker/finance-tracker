export interface SessionData {
  id: string;
  [key: string]: any;
}

export interface Session {
  data: SessionData | string;
  expiresAt: Date | string;
  id: string;
  createdAt: Date | string;
}
