const SUPER_ADMIN = 'super_admin';
const USER = 'user';

export const Role = {
  SUPER_ADMIN,
  USER,
};

export const Roles = [Role.SUPER_ADMIN, Role.USER];

export const RoleNames = {
  [Role.SUPER_ADMIN]: 'Super Admin',
  [Role.USER]: 'User',
};

export function isValidRole(role: string): boolean {
  return Roles.includes(role);
}

export default Role;
