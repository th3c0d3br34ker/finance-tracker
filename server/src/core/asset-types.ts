const EQUITY = 'equity';
const DEBT = 'debt';

export const AssetType = {
  EQUITY,
  DEBT,
};

export const AssetTypes = [EQUITY, DEBT];

export function isAssetType(assetType: string): boolean {
  return Object.values(AssetTypes).includes(assetType);
}

export default AssetTypes;
