const BANK = 'bank';
const WALLET = 'wallet';
const CASH = 'cash';

export const AccountType = {
  BANK,
  CASH,
  WALLET,
};

export const AccountTypes = [BANK, CASH, WALLET];

export function isValidAccountType(accountType: string): boolean {
  return Object.values(AccountTypes).includes(accountType);
}

export default AccountTypes;
