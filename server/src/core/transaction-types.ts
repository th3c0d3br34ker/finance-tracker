const INCOME = 'income';
const EXPENSE = 'expense';

export const TransactionType = {
  INCOME,
  EXPENSE,
};

export const TransactionTypes = [INCOME, EXPENSE];

export function isValidTransactionType(transactionType: string): boolean {
  return Object.values(TransactionTypes).includes(transactionType);
}

export default TransactionTypes;
