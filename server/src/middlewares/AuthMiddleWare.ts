import { NextFunction, Request, Response } from 'express';
import _ from 'lodash';
import { Roles } from '../core/roles';
import { BadRequestError, UnauthenticatedError } from '../errors/api.error';
import logger from '../logger';
import { IUserSessionService, USER_SESSION_NAME, USER_SESSION_SIGN } from '../services/user-session.service';
import { BaseMiddleware } from './BaseMiddleware';

export class AuthHeaderMiddleware extends BaseMiddleware {
  constructor() {
    super();
  }

  protected async handle(req: Request, _res: Response, next: NextFunction): Promise<void> {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      if (token) {
        (req as any).token = token;
        next();
        return;
      }
    }
    next(new UnauthenticatedError('No Authorization Header Found'));
  }
}

export class NoSessionMiddleware extends BaseMiddleware {
  constructor() {
    super();
  }
  protected async handle(req: Request, _res: Response, next: NextFunction): Promise<void> {
    if (!_.isNil(req.cookies[USER_SESSION_NAME]) && !_.isNil(req.cookies[USER_SESSION_SIGN])) {
      next(new BadRequestError('Session Already Exists'));
      return;
    }
    next();
  }
}

export class UserSessionMiddleWare extends BaseMiddleware {
  private readonly _sessionService: IUserSessionService;
  constructor(sessionService: IUserSessionService) {
    super();
    this._sessionService = sessionService;
  }

  protected async handle(req: Request, _res: Response, next: NextFunction): Promise<void> {
    if (!_.isNil(req.cookies[USER_SESSION_NAME]) && !_.isNil(req.cookies[USER_SESSION_SIGN])) {
      let session = null;
      try {
        session = await this._sessionService.getUserBySession(
          req.cookies[USER_SESSION_NAME],
          req.cookies[USER_SESSION_SIGN],
        );
      } catch (e) {
        logger.error('seession-middleware', e);
      }
      if (session) {
        (req as any).user = session;
      } else {
        (req as any).user = null;
      }
    } else {
      (req as any).user = null;
    }
    next();
  }
}

export class AllowRoleOnlyMiddleware extends BaseMiddleware {
  private readonly _roles: string[];
  constructor(roles: string[]) {
    super();
    this._roles = roles;
  }

  protected async handle(req: Request, _res: Response, next: NextFunction): Promise<void> {
    console.log(req.body);
    if (!_.isNil((req as any).user) && !_.isNil((req as any).user.role)) {
      if (this._roles.includes((req as any).user.role)) {
        next();
      } else {
        next(new UnauthenticatedError('User is not authorized'));
      }
    } else {
      next(new UnauthenticatedError('User is not authenticated'));
    }
  }
}

export class DontAllowRolesMiddleware extends BaseMiddleware {
  private readonly _roles: string[];
  constructor(roles: string[]) {
    super();
    this._roles = roles;
  }

  protected async handle(req: Request, _res: Response, next: NextFunction): Promise<void> {
    if (!_.isNil((req as any).user) && !_.isNil((req as any).user.role)) {
      if (!this._roles.includes((req as any).user.role)) {
        next();
      } else {
        next(new UnauthenticatedError('User is not authorized'));
      }
    } else {
      next(new UnauthenticatedError('User is not authenticated'));
    }
  }
}

export class OnlyLoggedInMiddleware extends BaseMiddleware {
  private readonly _roles: string[];
  constructor() {
    super();
    this._roles = Roles;
  }

  protected async handle(req: Request, _res: Response, next: NextFunction): Promise<void> {
    if (!_.isNil((req as any).user) && !_.isNil((req as any).user.id) && !_.isNil((req as any).user.role)) {
      if (this._roles.includes((req as any).user.role)) {
        next();
      } else {
        next(new UnauthenticatedError('User is not authenticated'));
      }
    } else {
      next(new UnauthenticatedError('User is not authenticated'));
    }
  }
}
