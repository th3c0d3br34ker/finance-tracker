import * as dotenv from 'dotenv';

dotenv.config({ override: true, debug: true });

export const SERVER_CONTEXT = 'EXPRESS SERVER';
export const DATABASE_CONTEXT = 'EXPRESS DATABASE';
export const EXPRESS_REQUEST_CONTEXT = 'EXPRESS REQUEST INFO';

export const HOST = process.env.HOST || 'localhost';

export const PORT = Number.parseInt(process.env.PORT || '3050');

export const NODE_ENV = process.env.NODE_ENV || 'development';

export const IS_TEST = NODE_ENV === 'test' || false;

export const DATABASE_URL = process.env.DATABASE_URL;

export const DB_NAME = process.env.DB_NAME || ':memory:';

export const DB_USER = process.env.DB_USER || 'postgres';

export const DB_PASSWORD = process.env.DB_PASSWORD || 'mysecretpassword';

export const SYNC_DB = process.argv.includes('--reset-db') || process.env.SYNC_DB_FORCE === 'true';

export const DB_SSL = process.env.DB_SSL === 'true';

export const SUPER_ADMIN_EMAIL = process.env.SUPER_ADMIN_EMAIL || 'admin@email.com';

export const SUPER_ADMIN_PASSWORD = process.env.SUPER_ADMIN_PASSWORD || 'th34dm1n';
