export const USER_TABLE_NAME = 'users';
export const USER_MODEL_NAME = 'User';

export const SESSION_TABLE_NAME = 'sessions';
export const SESSION_MODEL_NAME = 'Session';

export const ACCOUNTS_TABLE_NAME = 'accounts';
export const ACCOUNTS_MODEL_NAME = 'Account';
export const CREDIT_ACCOUNTS_TABLE_NAME = 'credit_accounts';
export const CREDIT_ACCOUNTS_MODEL_NAME = 'CreditAccount';

export const TRANSACTIONS_TABLE_NAME = 'transactions';
export const TRANSACTIONS_MODEL_NAME = 'Transaction';

export const CATEGORIES_TABLE_NAME = 'categories';
export const CATEGORIES_MODEL_NAME = 'Categories';

export const PORTFOLIO_TABLE_NAME = 'portfolios';
export const PORTFOLIO_MODEL_NAME = 'Portfolio';

export const ASSET_TABLE_NAME = 'assets';
export const ASSET_MODEL_NAME = 'Asset';

export const ModelNames = {
  USER: USER_MODEL_NAME,
  ACCOUNTS: ACCOUNTS_MODEL_NAME,
  CREDIT_ACCOUNT: CREDIT_ACCOUNTS_MODEL_NAME,
  TRANSACTIONS: TRANSACTIONS_MODEL_NAME,
  CATEGORIES: CATEGORIES_MODEL_NAME,
  PORTFOLIO: PORTFOLIO_MODEL_NAME,
  ASSET: ASSET_MODEL_NAME,
};

export const TableNames = {
  USER: USER_TABLE_NAME,
  ACCOUNTS: ACCOUNTS_TABLE_NAME,
  CREDIT_ACCOUNT: CREDIT_ACCOUNTS_TABLE_NAME,
  TRANSACTIONS: TRANSACTIONS_TABLE_NAME,
  CATEGORIES: CATEGORIES_TABLE_NAME,
  PORTFOLIO: PORTFOLIO_TABLE_NAME,
  ASSET: ASSET_TABLE_NAME,
};
