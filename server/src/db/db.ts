import path from 'path';
import fs from 'fs';
import _ from 'lodash';
import { DataTypes, Dialect, Sequelize } from 'sequelize';

import * as config from '../config';
import databaseConfig from './config';
import logger from '../logger';

const env = config.NODE_ENV;
const dbConfig = databaseConfig[env as keyof typeof databaseConfig];
const modelsPath = path.join(__dirname, 'models');
const models = Object.create(null);

logger.info(config.DATABASE_CONTEXT, `env: ${env}`);
logger.info(config.DATABASE_CONTEXT, `using database: ${dbConfig.database}`);

let sequelize: Sequelize;

if (_.isEmpty(config.DATABASE_URL) || _.isUndefined(config.DATABASE_URL)) {
  sequelize = new Sequelize({
    ...dbConfig,
    dialect: dbConfig.dialect as Dialect,
    database: dbConfig.database,
    logging: (...msg) => logger.debug(config.DATABASE_CONTEXT, msg[0]),
  });
} else {
  sequelize = new Sequelize(config.DATABASE_URL, {
    database: dbConfig.database,
    dialectOptions: {
      ...(config.DB_SSL
        ? {
            ssl: {
              require: true,
              rejectUnauthorized: false,
            },
          }
        : {}),
    },
  });
}

fs.readdirSync(modelsPath).forEach(async (file: string) => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const model = require(path.join(__dirname, 'models', file)).default(sequelize, DataTypes);
  models[model.name as string] = model;
});

Object.keys(models).forEach((modelName: string) => {
  if (models[modelName].associate && typeof models[modelName].associate === 'function') {
    models[modelName].associate(models);
  }
});

const db = {
  sequelize,
  Sequelize,
};

export default db;
