import * as config from '../config';

export default {
  development: {
    database: config.DB_NAME,
    dialect: 'sqlite',
    storage: config.DB_NAME,
  },
  test: {
    username: 'root',
    password: undefined,
    database: 'database_test',
    host: '127.0.0.1',
    dialect: 'mysql',
  },
  production: {
    username: 'root',
    password: undefined,
    database: 'database_production',
    host: '127.0.0.1',
    dialect: 'mysql',
  },
};
