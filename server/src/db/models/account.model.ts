import { Model, Sequelize, DataTypes, Optional } from 'sequelize';
import { Account } from '../../core/entities/account';
import AccountTypes from '../../core/accounts-types';

import { ACCOUNTS_MODEL_NAME, ACCOUNTS_TABLE_NAME, ModelNames } from '../constants';

type AccountCreationAttributes = Optional<Account, 'id'>;

function makeAccountsModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class AccountModel extends Model<Account, AccountCreationAttributes> implements Account {
    id!: string;
    name!: string;
    type!: string;
    user!: string;
    accountNumber!: string;
    openingBalance!: number;
    balance!: number;
    createdAt!: string | Date;
    updatedAt!: string | Date;
    deletedAt!: string | Date | null;

    static associate(models: any): void {
      AccountModel.belongsTo(models[ModelNames.USER], {
        foreignKey: 'user',
        targetKey: 'id',
      });
    }

    public toJSON(): object {
      const values = Object.assign({}, this.get());
      return values;
    }
  }

  AccountModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      type: {
        type: dataTypes.ENUM,
        values: AccountTypes,
        defaultValue: 'cash',
        allowNull: false,
      },
      accountNumber: {
        type: dataTypes.STRING,
        field: 'account_number',
      },
      openingBalance: {
        type: dataTypes.FLOAT,
        field: 'opening_balance',
        defaultValue: 0,
      },
      balance: {
        type: dataTypes.FLOAT,
        defaultValue: 0,
      },
      user: {
        type: dataTypes.UUID,
        allowNull: false,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: ACCOUNTS_MODEL_NAME,
      tableName: ACCOUNTS_TABLE_NAME,
      timestamps: false,
    },
  );

  return AccountModel;
}

export default makeAccountsModel;
