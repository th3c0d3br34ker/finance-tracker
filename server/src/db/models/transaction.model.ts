import { DataTypes, Model, Sequelize, Optional } from 'sequelize';
import { Transaction } from '../../core/entities/transaction';
import TransactionTypes from '../../core/transaction-types';

import { TRANSACTIONS_TABLE_NAME, TRANSACTIONS_MODEL_NAME, ModelNames } from '../constants';

type TransactionCreationAttributes = Optional<Transaction, 'id'>;

function makeTransactionsModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class TransactionModel extends Model<Transaction, TransactionCreationAttributes> implements Transaction {
    id!: string;
    type!: string;
    description!: string;
    amount!: number;
    account!: string;
    category!: string;
    recurring!: boolean;
    createdAt!: string | Date;
    updatedAt!: string | Date;
    deletedAt!: string | Date | null;

    static associate(models: any): void {
      TransactionModel.belongsTo(models[ModelNames.ACCOUNTS], {
        foreignKey: 'account',
        targetKey: 'id',
      });
      TransactionModel.belongsTo(models[ModelNames.CREDIT_ACCOUNT], {
        foreignKey: 'account',
        targetKey: 'id',
      });
      TransactionModel.belongsTo(models[ModelNames.CATEGORIES], {
        foreignKey: 'category',
        targetKey: 'id',
      });
    }

    public toJSON(): object {
      const values = Object.assign({}, this.get());
      return values;
    }
  }

  TransactionModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      type: {
        type: dataTypes.ENUM,
        values: TransactionTypes,
        allowNull: false,
      },
      amount: {
        type: dataTypes.FLOAT,
        allowNull: false,
      },
      description: {
        type: dataTypes.STRING,
      },
      recurring: {
        type: dataTypes.BOOLEAN,
        defaultValue: false,
      },
      account: {
        type: dataTypes.UUID,
      },
      category: {
        type: dataTypes.UUID,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
      },
    },
    {
      sequelize,
      modelName: TRANSACTIONS_MODEL_NAME,
      tableName: TRANSACTIONS_TABLE_NAME,
      timestamps: false,
    },
  );

  return TransactionModel;
}

export default makeTransactionsModel;
