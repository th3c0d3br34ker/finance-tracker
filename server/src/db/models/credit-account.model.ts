import { Model, Sequelize, DataTypes, Optional } from 'sequelize';
import { CreditAccount } from '../../core/entities/credit-account';

import { CREDIT_ACCOUNTS_MODEL_NAME, CREDIT_ACCOUNTS_TABLE_NAME, ModelNames } from '../constants';

type CreditAccountCreationAttributes = Optional<CreditAccount, 'id'>;

function makeAccountsModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class CreditAccountModel extends Model<CreditAccount, CreditAccountCreationAttributes> implements CreditAccount {
    id!: string;
    name!: string;
    type!: string;
    user!: string;
    accountNumber!: string;
    creditLimit!: number;
    balance!: number;
    createdAt!: string | Date;
    updatedAt!: string | Date;
    deletedAt!: string | Date | null;

    static associate(models: any): void {
      CreditAccountModel.belongsTo(models[ModelNames.USER], {
        foreignKey: 'user',
        targetKey: 'id',
      });
    }

    public toJSON(): object {
      const values = Object.assign({}, this.get());
      return values;
    }
  }

  CreditAccountModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      type: {
        type: dataTypes.STRING,
        defaultValue: 'credit-card',
        allowNull: false,
      },
      accountNumber: {
        type: dataTypes.STRING,
        field: 'account_number',
      },
      creditLimit: {
        type: dataTypes.FLOAT,
        field: 'credit_limit',
        defaultValue: 0,
      },
      balance: {
        type: dataTypes.FLOAT,
        defaultValue: 0,
      },
      user: {
        type: dataTypes.UUID,
        allowNull: false,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: CREDIT_ACCOUNTS_MODEL_NAME,
      tableName: CREDIT_ACCOUNTS_TABLE_NAME,
      timestamps: false,
    },
  );

  return CreditAccountModel;
}

export default makeAccountsModel;
