import { Model, Sequelize, DataTypes, Optional } from 'sequelize';
import { Category } from '../../core/entities/category';
import TransactionTypes from '../../core/transaction-types';
import { CATEGORIES_TABLE_NAME, CATEGORIES_MODEL_NAME } from '../constants';

type CategoryCreationAttributes = Optional<Category, 'id'>;

function makeCategoriesModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class CategoriesModel extends Model<Category, CategoryCreationAttributes> implements Category {
    id!: string;
    name!: string;
    type!: string;
    description!: string;
    color!: string;
    slug!: string;
    parent!: string | null;
    createdAt!: string | Date;
    updatedAt!: string | Date;
    deletedAt!: string | Date | null;

    static associate(): void {
      // relate a category to its parent
      CategoriesModel.belongsTo(CategoriesModel, {
        foreignKey: 'parent',
        targetKey: 'id',
      });

      CategoriesModel.hasMany(CategoriesModel, {
        foreignKey: 'parent',
        as: 'subCategories',
      });
    }

    public toJSON(): object {
      const values = Object.assign({}, this.get());
      return values;
    }
  }

  CategoriesModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      type: {
        type: dataTypes.ENUM,
        values: TransactionTypes,
        allowNull: false,
      },
      description: {
        type: dataTypes.STRING,
      },
      color: {
        type: dataTypes.STRING,
      },
      slug: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      parent: {
        type: dataTypes.UUID,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: CATEGORIES_MODEL_NAME,
      tableName: CATEGORIES_TABLE_NAME,
      timestamps: false,
    },
  );

  return CategoriesModel;
}

export default makeCategoriesModel;
