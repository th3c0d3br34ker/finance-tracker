import { Model, Sequelize, DataTypes, Optional } from 'sequelize';
import { Session, SessionData } from '../../core/entities/session';
import { SESSION_TABLE_NAME, SESSION_MODEL_NAME } from '../constants';

type SessionCreationAttributes = Optional<Session, 'id'>;

function makeSessionModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class SessionModel extends Model<Session, SessionCreationAttributes> implements Session {
    public id!: string;
    public data!: SessionData | string;
    public expiresAt!: Date | string;
    public createdAt!: Date | string;
  }

  SessionModel.init(
    {
      id: {
        type: dataTypes.STRING,
        primaryKey: true,
      },
      data: {
        type: dataTypes.TEXT,
        allowNull: true,
      },
      expiresAt: {
        type: dataTypes.DATE,
        allowNull: false,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: SESSION_MODEL_NAME,
      tableName: SESSION_TABLE_NAME,
      timestamps: false,
    },
  );

  return SessionModel;
}

export default makeSessionModel;
