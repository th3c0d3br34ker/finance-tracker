import { Model, Optional, Sequelize, DataTypes } from 'sequelize';
import { User } from '../../core/entities/user';
import { USER_MODEL_NAME, USER_TABLE_NAME } from '../constants';

type UserCreationAttributes = Optional<User, 'id'>;

function makeUserModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class UserModel extends Model<User, UserCreationAttributes> implements User {
    public id!: string;
    public name!: string;
    public email!: string;
    public password!: string;
    public createdAt!: Date;
    public updatedAt!: Date;
    public deletedAt!: Date | null;

    static associate(): void {
      // create associations
    }
  }

  UserModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: USER_MODEL_NAME,
      tableName: USER_TABLE_NAME,
      timestamps: false,
    },
  );

  return UserModel;
}

export default makeUserModel;
