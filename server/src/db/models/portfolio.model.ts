import { Model, Sequelize, DataTypes, Optional } from 'sequelize';
import AssetEntity from '../../core/entities/asset';
import { Portfolio } from '../../core/entities/portfolio';
import { ModelNames, PORTFOLIO_MODEL_NAME, PORTFOLIO_TABLE_NAME } from '../constants';

type PortfolioCreationAttributes = Optional<Portfolio, 'id'>;

function makePortfolioModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class PortfolioModel extends Model<Portfolio, PortfolioCreationAttributes> implements Portfolio {
    id!: string;
    name!: string;
    assets?: AssetEntity[] | undefined;
    user!: string;
    createdAt!: string | Date;
    updatedAt!: string | Date;
    deletedAt!: string | Date | null;

    static associate(models: any): void {
      PortfolioModel.belongsTo(models[ModelNames.USER], {
        foreignKey: 'user',
        targetKey: 'id',
      });

      PortfolioModel.hasMany(models[ModelNames.ASSET], {
        foreignKey: 'portfolio',
        sourceKey: 'id',
      });
    }

    public toJSON(): object {
      const values = Object.assign({}, this.get());
      return values;
    }
  }

  PortfolioModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      user: {
        type: dataTypes.UUID,
        allowNull: false,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: PORTFOLIO_MODEL_NAME,
      tableName: PORTFOLIO_TABLE_NAME,
      timestamps: true,
      paranoid: true,
    },
  );

  return PortfolioModel;
}

export default makePortfolioModel;
