import { Model, Sequelize, DataTypes, Optional } from 'sequelize';
import { Asset } from '../../core/entities/asset';
import { ASSET_MODEL_NAME, ASSET_TABLE_NAME, ModelNames } from '../constants';
import AssetTypes from '../../core/asset-types';

type AssetCreationAttributes = Optional<Asset, 'id'>;

function makeAssetModel<T extends typeof DataTypes>(sequelize: Sequelize, dataTypes: T) {
  class AssetModel extends Model<Asset, AssetCreationAttributes> implements Asset {
    id!: string;
    name!: string;
    symbol!: string;
    price!: number;
    type!: string;
    quantity!: number;
    createdAt!: string | Date;
    updatedAt!: string | Date;
    deletedAt!: string | Date | null;

    static associate(models: any): void {
      AssetModel.belongsTo(models[ModelNames.PORTFOLIO], {
        foreignKey: 'portfolio',
        targetKey: 'id',
      });
    }

    public toJSON(): object {
      const values = Object.assign({}, this.get());
      return values;
    }
  }

  AssetModel.init(
    {
      id: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      symbol: {
        type: DataTypes.STRING(10),
        allowNull: false,
      },
      price: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false,
      },
      quantity: {
        type: dataTypes.INTEGER,
        allowNull: false,
      },
      type: {
        type: dataTypes.ENUM,
        values: AssetTypes,
        allowNull: false,
      },
      createdAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      updatedAt: {
        type: dataTypes.DATE,
        defaultValue: dataTypes.NOW,
      },
      deletedAt: {
        type: dataTypes.DATE,
        defaultValue: null,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: ASSET_MODEL_NAME,
      tableName: ASSET_TABLE_NAME,
      timestamps: true,
      paranoid: true,
    },
  );

  return AssetModel;
}

export default makeAssetModel;
